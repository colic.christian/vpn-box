\section{Raspberry Pi}
\label{sec:umsetzung_raspberry}

Um die Erfahrung für den Remote-Access End-User möglichst einfach zu gestalten, wird ein Raspberry Pi eingesetzt. 
Das neuste Model 4B~\cite{raspberrypi.org:model4b-specs} unterstützt unter anderem Gigabit Ethernet und 802.11ac Wireless. 
Die Idee ist es, den Raspberry für den User soweit vorzubereiten, dass es für ihn eine Art Plug-and-Play VPN-Lösung wird. 
Er soll den Raspberry nur noch einstecken müssen, dabei wird die VPN Verbindung automatisch aufgebaut und es wird eine Wireless SSID ausgestrahlt, auf welche er sich mit seinen Endgeräten verbinden kann.


Von der BFH wurden uns zwei Raspberry Pi Model 4B zur Verfügung gestellt. Diese wurden sofort in Betrieb genommen und die neuste Version vom Raspbian Betriebssystem~\cite{raspberrypi.org:downloads} installiert.


\subsection{Installation und Basis-Setup von Raspbian}
\label{subsec:umsetzung_raspberry_installation}
Zuerst wurde das offizielle Raspbian Image auf den Raspberries installiert.
Dies gestaltete sich als etwas mühsamer als der Setup einer VM. 
Entweder muss man einen Setup mit Monitor und Tastatur aufsetzen oder man verändert das Installations-Image so weit, dass SSH nach dem Start automatisch aktiviert ist. 
Nachdem der Raspberry gestartet hat, muss ausfindig gemacht werden, welche IP er via DHCP bezogen hat. Danach kann via SSH auf den Raspberry verbunden werden.
Doch auch da gibt es Stolpersteine: Beim Testing dieser Images passiert es schnell und das System schliesst einem aus und die SSH-Verbindung geht verloren. So muss meist mit einen Monitor und einer Tastatur untersucht werden, wo nun genau der Fehler liegt.

Auch hier wurden zuerst alle nötigen Software-Pakete installiert: Open vSwitch, Wireguard, hostapd, usw.

\subsection{Open vSwitch und Netzwerk-Konfiguration}
\label{subsec:umsetzung_raspberry_ovs}
Damit der Raspberry nicht nur Layer-3 VPNs mit Wireguard aufbauen kann, sondern auch L2-VPNs mit VXLAN, wird Open vSwitch als Grundlage genutzt. OVS ist bereits für Debian paketiert und somit kann es ohne Probleme installiert werden.~\cite{openvswitch.org:install_debian}

Auf dem Raspberry werden zwei OVS Bridges eingerichtet, welche später genutzt werden, um wahlweise L2 oder L3 VPN Szenarios umzusetzen. Die beiden Bridges heissen \q{br-wan} und \q{br-internal}.

Dieselben Probleme, die in Abschnitt \ref{sec:umsetzung_architektur_ovs_network_config} beschrieben sind, mussten auch auf dem Raspberry gelöst werden. Dazu kommt noch, dass die Lösung von der VPN-Box VM nicht übernommen werden konnte.

Raspbian ist ein Betriebssystem, welches auf Debian basiert und hat damit eine ganz andere Netzwerk-Konfiguration als CentOS.
Unter Raspbian werden die Interfaces und ihre Konfiguration im File /etc/network/interfaces erfasst.

Das sieht normalerweise so aus:
\begin{mdframed}[backgroundcolor=light-gray, roundcorner=10pt,leftmargin=1, rightmargin=1, innerleftmargin=25, innertopmargin=5,innerbottommargin=1, outerlinewidth=1, linecolor=light-gray]
	\begin{lstlisting}[language=Bash]
# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
auto eth0
iface eth0 inet dhcp

auto eth1
iface eth1 inet static
address 10.10.10.12/24
gateway 10.10.0.1
dns-search example.com
dns-nameservers 10.10.0.2
	\end{lstlisting}
\end{mdframed}

Auch hier gibt es entsprechende Dokumentation~\cite{github:ovs_debian_readme} von Open vSwitch, welche erklärt wie man OVS Bridges und Interfaces in Debian-basierten Betriebssystem konfiguriert.

Um die Konfiguration übersichtlicher zu gestalten, wird der Ordner /etc/network/interfaces.d/ genutzt.
Im File /etc/network/interfaces steht schlussendlich nur eine Zeile, welche die Files im genannten Ordner einliest.

Als Beispiel wurde am Ende die OVS Bridge \q{br-wan} mit dem File /etc/network/interfaces.d/br-wan konfiguriert:
\begin{mdframed}[backgroundcolor=light-gray, roundcorner=10pt,leftmargin=1, rightmargin=1, innerleftmargin=15, innertopmargin=5,innerbottommargin=1, outerlinewidth=1, linecolor=light-gray]
	\begin{lstlisting}[language=Bash]
# OVS Bridge BR-WAN
# IP-Config: DHCP
# Physical Ports: eth0
auto br-wan
allow-ovs br-wan
iface br-wan inet dhcp
ovs_type OVSBridge
ovs_ports eth0
	\end{lstlisting}
\end{mdframed}

Und der dazugehörige Port eth0 mit dem File /etc/network/interfaces.d/eth0:
\begin{mdframed}[backgroundcolor=light-gray, roundcorner=10pt,leftmargin=1, rightmargin=1, innerleftmargin=15, innertopmargin=5,innerbottommargin=1, outerlinewidth=1, linecolor=light-gray]
	\begin{lstlisting}[language=Bash]
# Physical port eth0 belongs to OVS bridge BR-WAN
auto eth0
allow-br-wan eth0
iface eth0 inet manual
ovs_bridge br-wan
ovs_type OVSPort
	\end{lstlisting}
\end{mdframed}

Diese Konfiguration sauber zu erstellen war sehr zeitraubend. Wie oben bereits erwähnt, ist es umständlicher als, wenn man mit einer VM herumprobiert. 
Oft kam es vor, dass die Netzwerk-Konfiguration schief ging und dabei die Verbindung zum Raspberry verloren ging. Unzählige Male musste der Raspberry neu gestartet werden.

\subsection{Routing und iptables}
Sehr ähnlich wie beim Setup der VPN-Box VM im Abschnitt \ref{sec:umsetzung_architektur_routing_iptables}, war auch das Vorgehen auf dem Raspberry.

Auf dem Raspberry musste ebenfalls IP-Forwarding eingeschaltet werden~\cite{archlinux.org:internet_sharing_ip-forwarding} und es wurde ein Standard-iptables-Ruleset installiert, welches unter anderem in der FORWARD Chain Verbindungen zulässt.~\cite{archlinux.org:internet_sharing_iptables}

Auch wurde ein NAT eingerichtet. Dies ist einerseits nötig, damit die WLAN-Clients via dem Raspberry Pi Ethernet Interface direkt ins Internet können, aber andererseits auch damit sie via der Wireguard Verbindung aufs VPN zugreifen können.

Die iptables nat-Tabelle sieht folgendermasse aus:
\begin{mdframed}[backgroundcolor=light-gray, roundcorner=10pt,leftmargin=1, rightmargin=1, innerleftmargin=15, innertopmargin=5,innerbottommargin=1, outerlinewidth=1, linecolor=light-gray]
	\begin{lstlisting}[language=Bash]
*nat
:PREROUTING ACCEPT [0:0]
:INPUT ACCEPT [0:0]
:POSTROUTING ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
-A POSTROUTING -o br-wan -j MASQUERADE
-A POSTROUTING -s 192.168.4.0/24 -o wg1 -j MASQUERADE
COMMIT
	\end{lstlisting}
\end{mdframed}

Weshalb das Source Network 192.168.4.0/24 hier statisch hinterlegt ist, wird im nächsten Abschnitt erklärt.

\subsection{Konfiguration von Hostapd}
\label{subsec:umsetzung_konfiguration_hostapd}
Die WLAN-Fähigkeit des Raspberry Pi war schlussendlich der Auslöser dafür, warum er überhaupt in diesem Projekt genutzt wird.
Die Idee ist es, dass der Remote-Access-VPN mit dem Raspberry hergestellt wird, und dann dem Endbenutzer via WiFi-Hotspot auf sehr einfache Weise zur Verfügung gestellt wird.

Um mit dem Raspberry Pi einen WiFi-Hotspot zu erstellen, wird das Programm hostapd genutzt.

hostapd wurde mithilfe der man-Pages und diversen Tutorials installiert und es konnte ein WiFi-Hotspot erstellt werden.
Die Konfiguration dafür sieht etwa so aus:
\begin{mdframed}[backgroundcolor=light-gray, roundcorner=10pt,leftmargin=1, rightmargin=1, innerleftmargin=25, innertopmargin=5,innerbottommargin=1, outerlinewidth=1, linecolor=light-gray]
	\begin{lstlisting}[language=Bash]
interface=wlan0
driver=nl80211
#bridge=br-internal
hw_mode=g
channel=7
# limit the frequencies used to those allowed in the country
ieee80211d=1
country_code=CH
ieee80211n=1
ieee80211ac=1
# QoS support
wmm_enabled=1
utf8_ssid=1
ssid=WiFi-VPN L3
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=SUPERSECRET
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
	\end{lstlisting}
\end{mdframed}

Hiermit wird ein 2.4GHz WLAN Hotspot erstellt, welcher die SSID \q{WiFi-VPN L3} hat und durch WPA2/PSK mit dem Passwort \q{SUPERSECRET} geschützt ist.


Warum Zeile 3 auskommentiert ist, wird später noch erläutert.

Neben dieser Konfiguration braucht es noch weitere Einstellungen. Die Clients die sich auf dieses WLAN verbinden, werden als allererstes versuchen eine IP via DHCP zu beziehen. Damit sie diese bekommen, muss der verwendete Raspberry Pi einerseits eine statische IP besitzen und andererseits einen DHCP Server ausführen, welcher auf die Anfragen der Clients antwortet.

In unserem Aufbau ist das physische WLAN Interface \q{wlan0} Teil der OVS Bridge \q{br-internal}. Darum muss die statische IP auf der OVS Bridge gesetzt werden.

Anschliessend wird dnsmasq installiert und folgende Konfiguration eingerichtet:

\begin{mdframed}[backgroundcolor=light-gray, roundcorner=10pt,leftmargin=1, rightmargin=1, innerleftmargin=15, innertopmargin=5,innerbottommargin=1, outerlinewidth=1, linecolor=light-gray]
	\begin{lstlisting}[language=Bash]
interface=br-internal
# DHCP Range and lease time
dhcp-range=192.168.4.2,192.168.4.200,255.255.255.0,1h
# Optionally set a domain name
#domain=example.com
# Set default gateway
dhcp-option=3,192.168.4.1
# Set DNS servers to announce
dhcp-option=6,1.1.1.1,8.8.8.8
	\end{lstlisting}
\end{mdframed}

Die standardmässige Konfiguration sieht vor, dass die beiden DNS-Server \pythoninline{1.1.1.1} und \pythoninline{8.8.8.8} verwendet werden. Falls der User, diese im Formular via Admin-Webinterface spezifiziert, werden diese gemäss User Spezifikation ersetzt und Richtung VPN-Box getunnelt. 
Auch der DHCP Range und die DHCP Option 3, der Default Gateway, werden dynamisch gesetzt.
Dies passiert dann mit folgendem Jinja-Template:

\begin{mdframed}[backgroundcolor=light-gray, roundcorner=10pt,leftmargin=1, rightmargin=1, innerleftmargin=15, innertopmargin=5,innerbottommargin=1, outerlinewidth=1, linecolor=light-gray]
	\begin{lstlisting}[language=Bash]
interface=br-internal
# DHCP Range and lease time
dhcp-range={{ dhcp_start }},{{ dhcp_end }},{{ dhcp_mask }},1h
# Optionally set a domain name
#domain=example.com
# Set default gateway
dhcp-option=3,{{ router_ip }}
# Set DNS servers to announce
dhcp-option=6,{% if wg_config["dns_ips"] %}{{ wg_config["dns_ips"] }}{% else %}1.1.1.1,8.8.8.8{% endif %}
	\end{lstlisting}
\end{mdframed}

Diese paar Zeilen konfigurieren einen simplen DHCP Server, welcher auf dem Interface br-internal läuft. Er vergibt IPs im definierten IP Range und die lease time beträgt eine Stunde.

Das Netzwerk, welches für den WiFi Hotspot genutzt wird und in welchem die Clients schlussendlich ihre DHCP IP erhalten, kann vom Administrator im Webinterface konfiguriert werden. Wird kein Wert gesetzt, so wird das Netzwerk 192.168.4.0/24 standardmässig genutzt. Die erste verfügbare Host-Adresse wird dem Raspberry Pi auf der Bridge br-internal vergeben. Der DHCP Range wird automatisch von der zweiten bis zur letzten verfügbaren Host-Adresse konfiguriert.
Dies bedeutet, dass ein Jinja Template für das Network Config File von Interface \q{br-internal} gebraucht wird, aber auch die Iptables Rules müssen dynamisch generiert werden, da in der NAT-Rule am Ende das Source-Network angepasst werden muss.

Es ist aber weiterhin möglich, dass es zu IP Konflikten kommt. Falls der User sein Netzwerk zu Hause ändert oder den Raspberry in einer anderen Netzwerkumgebung in Betrieb nimmt, wo genau dieses Subnetz bereits verwendet wird, dann wird der Setup nicht mehr funktionieren.
Um diese Problematik zu umgehen, könnte beispielsweise ein Bash-Script entwickelt werden.
Dieses Script würde die IP-Adresse, welche auf dem Ethernet Interface bezogen wurde mit derjenige des WiFi-Interfaces, auf Konflikte prüfen.
Wird ein Konflikt erkannt, könnte das Script die dnsmasq Einstellungen anpassen und die statische IP auf dem Interface br-internal anpassen.
Da diese Problematik erst im Laufe des Projektes bekannt wurde, haben wir diesen offenen Punkt im Kapitel Future-Work aufgenommen.

Falls es aktuell tatsächlich zu einem solchen IP Konflikt kommen, kann der Administrator die Konfiguration für diesen User anpassen und dabei das WiFi Subnetz anpassen. Danach kann er das neue SD-Karten Image herunterladen und neu auf den Raspberry Pi schreiben.

Der laufende Service für den Endbenutzer funktioniert wie folgt:
Clients verbinden sich auf das WLAN, welches vom Raspberry Pi vom Interface wlan0 ausgestrahlt wird.
Die Layer 2 Frames der Clients landen damit auf der OVS Bridge br-internal, wo dann die DHCP Requests vom dnsmasq Service beantwortet werden. Die Clients erhalten eine IP und können sich erfolgreich verbinden.


\subsection{Erste Tests}
Die ersten Tests verliefen problemlos. Es konnte eine Wireguard VPN Verbindung von einem der Raspberrys zu einem Zielserver in der Digitalocean Cloud hergestellt werden. Dann wurde mit hostapd ein WiFi Hotspot erstellt, auf welchen sich Clients, wie zum Beispiel Smartphones, verbinden können. Nach wenigen Troubleshooting Schritten und Anpassungen der iptables Rules, funktionierte die Verbindung.

Danach wurde ein L2-VPN Szenario getestet. Der Raspberry und der Server in der Digitalocean Cloud wurden so umkonfiguriert, dass eine Wireguard VPN Verbindung hergestellt wurde, und darüber noch ein Open vSwitch VXLAN Tunnel. Tests zwischen Server und Raspberry waren erfolgreich und der gesamte Layer2 Traffic der Gegenseite war ersichtlich. Doch als es darum ging den hostapd einzuschalten, gab es ein Problem.

Wie sich herausstellte, funktioniert hostapd nicht mit OVS Bridges, da Linux Bridges erwartet werden. 
Nach Recherchearbeiten auf diversen Webseiten und Foreneinträgen tauchte zum Glück eine Mailing List Diskussionen auf, welche hilfreiche Hinweise enthielt, um das Problem beheben zu können. Mehr dazu im Abschnitt \ref{sec:probleme_raspberrypi_hostapd}.

\subsection{Custom Raspbian Image mit Pi-Gen}
\label{sec:umsetzung_raspberry_pigen}
Die Installation, welche auf dem Raspberry Pi vorbereitet wurde, soll später im Admin Webinterface als Download zur Verfügung stehen. So kann der Administrator das Image herunterladen und auf eine SD-Karte schreiben. Der Raspberry kann dann dem End-User übergeben werden und ist bereits für das VPN vorkonfiguriert.

Nun ist es aber nicht ganz so einfach. Wenn ein Image der SD-Karte erstellt wird, so wird dieses 16GB gross sein, da den Raspberrys jeweils eine 16GB SD-Karte eingelegt ist. Zuerst war die Idee, das Image zu komprimieren, da der Grossteil nur leerer Speicherplatz ist. Das Image wird dadurch zwar viel kleiner, aber dies bringt andere Probleme mit sich: Das Programm muss schlussendlich in der Lage sein, das Image anhand der Formular-Eingaben des Administrators direkt anzupassen und dann als Download zur Verfügung zu stellen. Durch die Komprimierung ist dies nicht mehr so einfach möglich.

Es fiel auf, dass das offizielle Raspbian Lite Image nur 2.2GB gross ist. Bei der Recherche stiessen wir schlussendlich auf das Tool Pi-Gen~\cite{github.com:pi-gen}, welches genutzt wird, um die offiziellen Raspbian Images zu bauen. Dies wurde so erweitert, dass alle VPN-Box spezifischen Installationsschritte auch ausgeführt werden und damit ein Custom Raspbian Image generiert wird, welches nur ca. 2GB gross ist.

Pi-Gen basiert auf sogenannten Stages, welche unterschiedliche Aufgaben haben. Man kann Files kopieren, Befehle ausführen, Pakete installieren etc.
Dies wurde genutzt und damit eine neue Stage definiert, welche die spezifischen Aufgaben übernimmt.

Kurz zusammengefasst, sieht das so aus:

\begin{mdframed}[backgroundcolor=light-gray, roundcorner=10pt,leftmargin=1, rightmargin=1, innerleftmargin=25, innertopmargin=5,innerbottommargin=1, outerlinewidth=1, linecolor=light-gray]
\begin{lstlisting}[language=Bash]
#!/bin/bash -e

install -m 644 files/raspberrypi2.gpg.key "${ROOTFS_DIR}/tmp/"
install -m 644 files/raspberrypi3.gpg.key "${ROOTFS_DIR}/tmp/"
install -m 644 files/unstable.list "${ROOTFS_DIR}/etc/apt/sources.list.d/"
install -m 644 files/limit-unstable "${ROOTFS_DIR}/etc/apt/preferences.d/"

install -m 644 files/sysctl.conf "${ROOTFS_DIR}/etc/sysctl.d/01-vpn-box.conf"

on_chroot << EOF
apt-key add - < /tmp/raspberrypi2.gpg.key
apt-key add - < /tmp/raspberrypi3.gpg.key
apt update
apt-get install -y openvswitch-switch wireguard dnsmasq iptables iptables-persistent python3 python3-pip python3-virtualenv hostapd
chmod 700 /etc/wireguard
systemctl disable dhcpcd
systemctl disable hostapd
systemctl enable networking
systemctl enable netfilter-persistent
systemctl enable openvswitch-switch
systemctl enable wg-quick@wg1
EOF
# systemctl enable dnsmasq
# systemctl enable hostapd@l2
# systemctl enable hostapd@l3

# Overwrite the hostapd@.service file with our modified one
install -m 644 files/hostapd.service "${ROOTFS_DIR}/lib/systemd/system/hostapd@.service"

# Copy our patched hostapd binaries
install -m 755 files/hostapd "${ROOTFS_DIR}/usr/sbin/hostapd"
install -m 755 files/hostapd_cli "${ROOTFS_DIR}/usr/sbin/hostapd_cli"

# Copy two hostapd configs
install -m 644 files/hostapd_l2.conf "${ROOTFS_DIR}/etc/hostapd/l2.conf"
install -m 644 files/hostapd_l3.conf "${ROOTFS_DIR}/etc/hostapd/l3.conf"

# Copy dnsmasq config
install -m 644 files/dnsmasq.conf "${ROOTFS_DIR}/etc/dnsmasq.conf"

# Copy network interfaces and ovs bridge configs
install -m 644 files/interfaces/interfaces "${ROOTFS_DIR}/etc/network/interfaces"
install -m 644 files/interfaces/br-wan "${ROOTFS_DIR}/etc/network/interfaces.d/br-wan"
install -m 644 files/interfaces/br-internal "${ROOTFS_DIR}/etc/network/interfaces.d/br-internal"
install -m 644 files/interfaces/eth0 "${ROOTFS_DIR}/etc/network/interfaces.d/eth0"
install -m 644 files/interfaces/wlan0 "${ROOTFS_DIR}/etc/network/interfaces.d/wlan0"

# Copy iptables rulesets
install -m 644 files/iptables.rules.v4 "${ROOTFS_DIR}/etc/iptables/rules.v4"
install -m 644 files/iptables.rules.v6 "${ROOTFS_DIR}/etc/iptables/rules.v6"

\end{lstlisting}
\end{mdframed}

Die Custom-Stage kopiert einige Files ins Image, installiert alle Pakete, die benötigt werden und aktiviert alle Services, welche genutzt werden.

Schliesslich werden noch einige Optionen im File \q{config} definiert und danach der Build-Prozess gestartet.
Unter anderem werden dort Informationen wie der Hostnamen des Systems, das Keyboard-Mapping, die Zeitzone, das Passwort des initialen Users \q{pi} definiert und der SSH Service wird aktiviert.

Nach mehreren Testläufen und Versuchen, war der Aufbau so weit, dass ein Funktionierendes Custom VPN-Box Raspbian Image generiert wurde.
Das Image ist ca. 2.5GB gross und wird unkomprimiert auf der VPN-Box abgelegt. Die Web-Applikation kann nun das Image dynamisch mounten, Änderungen an den Config Files vornehmen und das Image schliesslich als Download zur Verfügung stellen.
