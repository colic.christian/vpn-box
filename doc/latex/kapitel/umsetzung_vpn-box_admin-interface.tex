\section{VPN-Box Admin-Interface}
\label{sec:umsetzung_vpn-box_admin-interface}
Die Hauptsteuerzentrale für jegliche Funktionen und Automatisierungen ist das VPN-Box Admin-Interface, dies wurde mit dem Python Flask Webframework, wie in \ref{sec:umsetzung_flask} beschrieben, implementiert.

In den nächsten Abschnitten werden die wichtigsten Funktionen, die entwickelt wurden, erklärt. Ausserdem, wird erläutert welche Komponenten mit der Automatisierung angesteuert werden.

\subsection{Home Page}
\label{subsec:umsetzung_home-page}
Wenn sich der Administrator auf dem Admin-Interface einloggt, wird er automatisch auf die Home Page weitergeleitet. Von dort aus, werden ihm die wichtigsten Links angezeigt, um den Userguide oder Adminguide herunterzuladen, eine statische IP-Konfiguration vorzunehmen und ein RA- oder ein S2S-VPN in Betrieb zu nehmen. Ausserdem wird geprüft, ob der Administrator immer noch die \q{default credentials} nutzt, wenn ja wird ihm ein roter Warnbutton angezeigt, dass er das Admin-Passwort ändern sollte. Dazu gibt es via Settings Page die Möglichkeit dies zu erledigen.

\subsection{Konfigurations Pages}
\label{subsec:umsetzung_config-pages}
Die verschiedenen Konfigurations Pages zeichnen, die möglichen Konfigurationen, die auf der VPN-Box via Webinterface vorgenommen werden können, ab. Um dies zu ermöglichen werden Formulare angeboten, um den User-Input zu prüfen und verarbeiten. Mehr dazu in den Folgeabschnitten.


\subsubsection{S2S und RA Formulare}
Zu Beginn der S2S und RA Konfiguration hat der Administrator die Auswahl, ob er eine Layer 3 oder eine Layer 2 Konfiguration einrichten möchte. Dies wird mit einem grossen \q{Toggle-Button} bestimmt.
Es gibt übersichtliche Formulare, mit welchen der Admin einfach ein Site-to-Site oder Remote-Access VPN einrichten kann. Seine Eingaben werden validiert und persistent in einer Datenbank gespeichert.

Falls der User eine kleine Anpassung am Setup machen muss, können eingegebenen Daten bearbeitet werden. Dabei wird das Formular, mit den bestehenden Daten, aus der Datenbank befüllt und der Admin muss nur die Felder anpassen, welche sich auch wirklich verändern. Nach dem Speichern dieser Änderungen, werden diese direkt im System appliziert.

\subsubsection{RA-User Formular}
Es existiert ein Formular, um neue Benutzer zum Remote-Access VPN hinzuzufügen. Die User werden anschliessend in einer Tabelle aufgelistet, wo diese bearbeitet oder gelöscht werden können.

\subsection{Static-IP Formular}
Beim ersten Start der VPN-Box wird per DHCP eine dynamische IP bezogen. Nachdem sich der Admin im Webinterface eingeloggt hat, besteht die Möglichkeit via Formular eine statische IP zu setzen.

\subsubsection{Formular Validierung}
Die Formvalidierung geschieht mit sogenannten \q{validators}, einige sind ziemlich einfach und prüfen lediglich, ob der Formularinhalt beispielsweise zwischen 1 und 65535 liegt. Andere Wiederum prüfen, ob eine IPv4 Adresse, IPv6 Adresse, CIDR Notation in IPv4 oder IPv6 vorhanden ist, diese sind eher kompliziert.

Anbei ein Beispiel der Remote-Access Form, wo IP-Adressen, Ports, Subnetze für die einzelnen Dienste eingegeben werden müssen. Zum Teil dürfen diese Felder auch leer bleiben.

\begin{mdframed}[backgroundcolor=light-gray, roundcorner=10pt,leftmargin=1, rightmargin=1, innerleftmargin=25, innertopmargin=5,innerbottommargin=1, outerlinewidth=1, linecolor=light-gray]
	\begin{lstlisting}[language=Python]
class RemoteAccessForm(FlaskForm):
	class Meta:
		model = RAConfig
		exclude = ['time_created']

	a_public_ip = StringField("Public IP", validators=[check_ip])
	a_public_port = IntegerField("Public Port", validators=[
	NumberRange(min=1, max=65535, message="Port number not in range (1-65535)")])
	a_tunnel_cidr = StringField("Tunnel IP + Mask (CIDR)", validators=[check_ip_cidr])
	a_local_networks = StringField("Local Networks", validators=[check_local_networks])
	dns1 = StringField("DNS 1 IP", validators=[check_ip_or_emptystring])
	dns2 = StringField("DNS 2 IP", validators=[check_ip_or_emptystring])
	l3 = BooleanField()
	submit = SubmitField('Upload new RA config-parameters')
	\end{lstlisting}
\end{mdframed}

All die oben genannten Bedingungen müssen erfüllt werden, ansonsten erscheint auf der Website ein Formularfehler, und die Eingabe kann nicht bestätigt werden. Oft bauen solche Checks auf Regulären Ausdrücken (Regex) auf, zwei sind unten aufgeführt. Diese prüfen, ob eine IPv4 Netzmaske stimmt und ob die IPv4-Adresse in korrekter \gls{CIDR} Notation ist. Es wird bewusst darauf verzichtet, die langen IPv6-Regexe hier aufzuzeigen.

Manchmal ist es notwendig interne Überschneidungen von Netzwerkparametern zu prüfen. Beispielsweise sollte die Adressierung des S2S-Setup nicht mit dem RA-Setup überschneidend sein. Hingegen soll sichergestellt werden, dass die gewählten benutzerspezifischen IP-Adressen für den Remote-Access im, für diesen Setup definierten, Tunnel IP-Range liegen.

\begin{mdframed}[backgroundcolor=light-gray, roundcorner=10pt,leftmargin=1, rightmargin=1, innerleftmargin=15, innertopmargin=5,innerbottommargin=1, outerlinewidth=1, linecolor=light-gray]
	\begin{lstlisting}[language=Python]
# Validate IPv4 netmask
regex_ipv4_netmask = (
"^(((255\.){3}(255|254|252|248|240|224|192|128|0+))|((255\.){2}("
"255|254|252|248|240|224|192|128|0+)\.0)|((255\.)(255|254|252|248|240|224|192|128|0+)(\.0+){"
"2})|((255|254|252|248|240|224|192|128|0+)(\.0+){3}))$")

# Validate IPv4 in prefix (CIDR) format
regexp_ipv4_cidr = r'^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2]))?$'
	\end{lstlisting}
\end{mdframed}

\subsection{Informational Pages}
\label{subsec:umsetzung_vpn-box_status-page}
Zu jeder Konfiguration, die getätigt werden kann, existiert eine Status Page, die alle wichtigen Information dazu zeigt. Dabei werden alle nötigen Infos aus der Datenbank gelesen oder direkt vom System (WireGuard, Open vSwitch, Linux, etc.) abgefragt.

\subsubsection{Status Page}
Auf der Hauptstatus Page werden jegliche Informationen über bestehende Wireguard Interfaces abgebildet, dies sowohl für S2S, RA wie auch OVS.

Es kann z.B. geprüft werden, wie viel Traffic total empfangen und versendet wurde, wann sich ein RA-Client das letzte Mal gemeldet hat (Handshake), wer verbunden ist (Username), welche Endpoint IP er besitzt und vieles mehr.

\subsubsection{Import / Export der Site-to-Site Konfiguration}
Nachdem der Admin eine Site-to-Site Konfiguration erstellt und gespeichert hat, kann er diese exportieren. Auf der entsprechenden Gegenseite kann diese Konfiguration dann importiert werden. Dies verhindert das, ansonsten mühsame, manuelle eingeben der Konfigurationsparameter.

\subsubsection{System Logs Page}
Auf der Systems Log Page wird dem Administrator die Möglichkeit geboten, Logs der folgenden Komponenten anzuzeigen:

\begin{itemize}
	\item Open vSwitch
	\item VPN-Box
	\item iptables
	\item Linux Syslog
\end{itemize}

So hat er die Möglichkeit nachzuvollziehen, was auf dem System ausgeführt wird, oder warum eine Verbindung auf die VPN-Box nicht klappt. So prüft er vermutlich das iptables Log.


\subsection{Konfiguration der VPN-Box}
\label{subsec:umsetzung_vpn-box_config-of-the-box}
Die Web-App hat die Möglichkeit direkt auf der VPN-Box Kommandozeilen-Befehle auszuführen. Dies wird an verschiedenen Orten genutzt, um Konfigurationen vorzunehmen oder Daten auszulesen.

Genau so wird dies für die folgenden zwei beschriebenen Komponentenkonfigurationen verwendet.

\subsubsection{Konfiguration von Wireguard}
Um die Konfiguration von Wireguard exakt vornehmen zu können, wurde eine Klassenstruktur erstellt, die es erlaubt von der Haupt-Wireguard Klasse zu erben. Auf zweiter ebene sind dann die Subklassen, WG\_RA, WG\_S2S und WG\_User, welche weitere spezifische Felder beinhalten. Jeder dieser Klassen benutzt die WgPeer Klasse, um entsprechende Wireguard-Peers abzubilden.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{bilder/umsetzung_software-architektur/wg_class-diagram.png}
	\caption{Wireguard Klassen-Diagramm}
	\label{fig:wg_class-diagram}
\end{figure}

Mit Hilfe dieser Struktur werden Daten aus einer SQLite Datenbank ausgelesen und in vorbereitete Jinja-Templates abgefüllt. Die daraus erzeugten Files werden anschliessend auf die VPN-Box oder auf das gemountete Raspberry-Image gepatched.

\subsubsection{Konfiguration von Open vSwitch}
Wie die Konfiguration von Wireguard, wird auch die OVS-Konfiguration mittels Daten aus dem Backend in Jinja-Templates abgefüllt und anschliessend das System mit den erzeugten Files gepatched.


\subsection{Raspberry Pi SD-Karten Image}
\label{subsec:umsetzung_vpn-box_rpi_sd-image}
Auf der VPN-Box ist das massgeschneidertes SD-Karten Image abgelegt. In Abschnitt \ref{sec:umsetzung_raspberry_pigen} ist genauer beschrieben, wie das Image generiert wurde.

Die Web-App kann das Image während des Betriebs mounten, um darin Änderungen vorzunehmen, wie z.B. ob das VPN-Setup im Layer 2 oder Layer 3 Modus aufgesetzt werden soll.

Dafür wurde eine Utility-Klasse geschrieben, welche die Zugriffe auf das Image abstrahiert. So muss nur angeben werden, welches File auf dem Image angepasst werden soll. Die Utility Klasse übernimmt das vollständige Handling des Files, inklusive \q{mount} und \q{unmount}, falls nötig.

Je nach dem welches Szenario gewünscht ist, müssen sogar Systemd Services auf dem Raspberry aktiviert oder deaktiviert werden. Dafür werden \q{symlinks} auf dem Image erstellt, auch dies wird von der Utility Klasse erledigt.

Schlussendlich kann das Image File als Download zur Verfügung gestellt werden. Der Admin kann es zu gegebener Zeit herunterladen, auf eine SD Karte schreiben und anschliessend in vorbereiteter Form an den Endbenutzer weitergeben.

\subsection{QR-Code for Smartphones}
\label{sec:umsetzung_qr-code}
Ziel ist es mittels dem QR-Code die Möglichkeit zu bieten Smartphones in das erstellte VPN einwählen zu lassen.
Um einem Mitarbeiter die Möglichkeit zu bieten mit dem Smartphone und einem Raspberry-Pi zur selben Zeit mit dem entsprechenden VPN verbunden zu sein, muss je ein User erstellt werden. Grund dafür ist die asymmetrische Kryptographie, die es absichert, dass mit einem erstellten Key nur eine gleichzeitige Verbindung aufgebaut werden darf.

Dank dem praktischen und einfachen Flask Web-Framework war, die Erstellung und das Anzeigen des QR-Codes, eine nicht all zu aufwändige Aufgabe. Somit benötigt man nur zwei weitere Python-Libraries, um dies zu implementieren. Die pyqrcode Library dient zum Erstellen des QR-Codes bestehend aus der Wireguard-Config, welche bereits für die Konfiguration von Raspberry-Pi genutzt wurde. Die zweite Library wird genutzt, um den QR-Code als PNG-Bild zu speichern.

\begin{mdframed}[backgroundcolor=light-gray, roundcorner=10pt,leftmargin=1, rightmargin=1, innerleftmargin=15, innertopmargin=5,innerbottommargin=1, outerlinewidth=1, linecolor=light-gray]
	\begin{lstlisting}[language=Python]
pyqrcode==1.2.1
pypng==0.0.20
	\end{lstlisting}
\end{mdframed}

Kurz erklärt:\newline
- Die controller Klasse bietet die User-spezifische URL, um die Generierung des QR-Codes anzustossen.


- Die setup Klasse generiert den entsprechenden Wireguard-User, daraus wird die Wireguard-Config abgeleitet und als String der util Klasse bzw. der definierten Methode übergeben.


- Die util Klasse generiert den QR-Code mit dem gegebenen Config-String und schreibt das QR-Code Bild in einen vordefinierten Ordner.

Die QR-Code Bilder werden nicht dauerhaft gespeichert, dies aus sicherheitstechnischen Aspekten und um nicht unnötig Speicherplatz zu brauchen.

\begin{mdframed}[backgroundcolor=light-gray, roundcorner=10pt,leftmargin=1, rightmargin=1, innerleftmargin=25, innertopmargin=5,innerbottommargin=1, outerlinewidth=1, linecolor=light-gray]
	\begin{lstlisting}[language=Python]
# controller
@url.route("/qrcode/user/<int:user_id>", methods=["GET"])
@login_required
def qrcode_user(user_id):
	# Generate QR Code
	SetupPhone.generate_qr_code(user_id)
	return render_template('qrcode.html')

# setup 
class SetupPhone:
	"""
	Helper class for setting up the Wireguard QR code configs
	"""
	
	@staticmethod
	def generate_qr_code(user_id):
		wg_user = WG_User(user_id)
		rendered = Template(open('admin_interface/config_templates/rpi/wg.conf.j2').read()).render(wg_config=wg_user)
		create_qr_code_image(rendered)

# util
def create_qr_code_image(file):
	qr = pyqrcode.create(file)
	qr.png("admin_interface/static/qrcode/wg_qrcode.png", scale=4)

	\end{lstlisting}
\end{mdframed}

Nachdem ein, via Admin-Webinterface erstellter, QR-Code auf dem Smartphone eingelesen wurde, sieht eine Konfiguration beispielsweise wie folgt aus:

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.15]{bilder/umsetzung_flask/qr_code_wg_phone.jpg}
	\caption{Smartphone Wireguard App Konfiguration}
	\label{fig:smartphone_wireguard}
\end{figure}





