#!/bin/bash -e

install -m 644 files/raspberrypi2.gpg.key "${ROOTFS_DIR}/tmp/"
install -m 644 files/raspberrypi3.gpg.key "${ROOTFS_DIR}/tmp/"
install -m 644 files/unstable.list "${ROOTFS_DIR}/etc/apt/sources.list.d/"
install -m 644 files/limit-unstable "${ROOTFS_DIR}/etc/apt/preferences.d/"

install -m 644 files/sysctl.conf "${ROOTFS_DIR}/etc/sysctl.d/01-vpn-box.conf"

on_chroot << EOF
apt-key add - < /tmp/raspberrypi2.gpg.key
apt-key add - < /tmp/raspberrypi3.gpg.key
apt update
apt-get install -y openvswitch-switch wireguard dnsmasq iptables iptables-persistent python3 python3-pip python3-virtualenv hostapd
chmod 700 /etc/wireguard
systemctl disable dhcpcd
systemctl disable hostapd
systemctl enable networking
systemctl enable netfilter-persistent
systemctl enable openvswitch-switch
systemctl enable wg-quick@wg1
EOF
# systemctl enable dnsmasq
# systemctl enable hostapd@l2
# systemctl enable hostapd@l3

# Overwrite the hostapd@.service file with our modified one
install -m 644 files/hostapd.service "${ROOTFS_DIR}/lib/systemd/system/hostapd@.service"

# Copy our patched hostapd binaries
install -m 755 files/hostapd "${ROOTFS_DIR}/usr/sbin/hostapd"
install -m 755 files/hostapd_cli "${ROOTFS_DIR}/usr/sbin/hostapd_cli"

# Copy two hostapd configs
install -m 644 files/hostapd_l2.conf "${ROOTFS_DIR}/etc/hostapd/l2.conf"
install -m 644 files/hostapd_l3.conf "${ROOTFS_DIR}/etc/hostapd/l3.conf"

# Copy dnsmasq config
install -m 644 files/dnsmasq.conf "${ROOTFS_DIR}/etc/dnsmasq.conf"

# Copy network interfaces and ovs bridge configs
install -m 644 files/interfaces/interfaces "${ROOTFS_DIR}/etc/network/interfaces"
install -m 644 files/interfaces/br-wan "${ROOTFS_DIR}/etc/network/interfaces.d/br-wan"
install -m 644 files/interfaces/br-internal "${ROOTFS_DIR}/etc/network/interfaces.d/br-internal"
install -m 644 files/interfaces/eth0 "${ROOTFS_DIR}/etc/network/interfaces.d/eth0"
install -m 644 files/interfaces/wlan0 "${ROOTFS_DIR}/etc/network/interfaces.d/wlan0"

# Copy iptables rulesets
install -m 644 files/iptables.rules.v4 "${ROOTFS_DIR}/etc/iptables/rules.v4"
install -m 644 files/iptables.rules.v6 "${ROOTFS_DIR}/etc/iptables/rules.v6"
