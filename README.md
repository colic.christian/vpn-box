# Bachelor Thesis - VPN Box

One Paragraph of project description goes here

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Download and deploy the VPN-Box VM

The VPN-Box is available as an OVA VM Template

You can find it in this Repository as an Git LFS File at: 

`dev/vpn-box-vm-template/vpn-box-master.ova`

Download it and deploy it in your favourite VM Hypervisor. VMware and Virtualbox works best.

### Prerequisites

What things you need to install the software and how to install them

- Python3.7

### Installing

A step by step series of examples that tell you how to get a development env running

Create virtualenv at vpn-box/venv
```
virtualenv venv
```

Install requirements
```
venv/bin/pip install -r src/requirements.txt
```

Run the development webserver

```
cd src
PYTHONPATH=$(pwd)/admin_interface ../venv/bin/python run.py
```

Run the production Gunicorn webserver
```
../venv/bin/gunicorn -c gunicorn.conf.py run:app
```


## Built With

* [Flask](https://palletsprojects.com/p/flask/) - The web framework used
* [SQLAlchemy](https://www.sqlalchemy.org/) - SQL Toolkit and ORM
* [WTForms](https://wtforms.readthedocs.io/en/stable/) - Forms Framework
* [Gunicorn](https://gunicorn.org/) - Python WSGI HTTP Server

## Authors

* **Marc Werenfels**
* **Christian Colic**
