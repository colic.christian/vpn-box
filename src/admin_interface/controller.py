import tempfile

from flask import render_template, Blueprint, flash, redirect, url_for, send_file
from flask_login import login_user, logout_user, login_required
from werkzeug.utils import secure_filename

from admin_interface import login_manager
from forms import *
from models import S2SConfig, RAConfig, UserModel, LoginModel, delete
from sd_util import SdUtil
from setup import SetupVM, SetupNetworking, SetupRPI, SetupPhone
from status import wg_status, ovs_status
from util import read_and_parse_s2s_config, WgQuick, SystemLogs, Iptables

url = Blueprint('url', __name__, )


@url.route('/')
def home():
    admin_user = LoginModel.query.get(1)
    password_bool = LoginModel.check_password(admin_user, "admin")
    return render_template("index.html", password_bool=password_bool, title='Welcome')


@url.route('/status')
@login_required
def status():
    return render_template("status.html", status={"wg": wg_status(), "ovs": ovs_status()}, title='Status Page')


@url.route('/download')
@login_required
def download():
    s = SdUtil()
    s.umount()
    return send_file(s.img_path, as_attachment=True)


@url.route('/download_guide/<string:filename>', methods=["GET", "POST"])
def download_guide(filename):
    logging.info("Exporting PDF Guide")
    img_path = "/opt/vpn-box/"
    file = img_path + filename

    # Read and serve the file as download
    return send_file(file, as_attachment=True)


@url.route("/setup_static_ip", methods=["GET", "POST"])
@login_required
def setup_static_ip():
    form = StaticIpForm()
    nw = SetupNetworking
    if form.validate_on_submit():
        nw.set_sys_network(form.data)
        flash('Static ip address has been set!', 'success')

    try:
        ip = nw.get_sys_ip("br-wan")
    except ValueError:
        return render_template("error.html", error_title="Error: Interface missing",
                               error_message="Interface 'br-wan' is missing!")
    bootproto = nw.get_sys_bootproto()
    gw = nw.get_sys_route()
    ns = nw.get_sys_dns()
    return render_template("static_ip_form.html", form=form, ip=ip, gw=gw, ns=ns, bootproto=bootproto,
                           title='Setup Static IP')


@url.route("/s2s_setup", methods=["GET", "POST"])
@login_required
def setup_s2s():
    # Load forms
    form_s2s = SiteToSiteSetupForm()
    upload_form = SiteToSiteImportForm()

    # Form was submitted
    if form_s2s.validate_on_submit():
        # Insert/Update to DB.
        S2SConfig().update_db_s2s(form_s2s)

        SetupVM.setup_s2s()

        flash("Config was saved and applied to the system!", "success")
        return redirect(url_for("url.setup_s2s"))
    return render_template('s2s_setup_form.html',
                           form=form_s2s,
                           upload_form=upload_form,
                           s2s_config=S2SConfig.query.get(1),
                           title='Site-to-Site Setup')


@url.route("/import_s2s", methods=["POST"])
@login_required
def import_s2s():
    import_form = SiteToSiteImportForm()
    if import_form.validate_on_submit():
        # safely store the file
        filename = secure_filename(import_form.config_file.data.filename)
        import_form.config_file.data.save('uploads/' + filename)

        try:
            # read and parse the s2s config file
            js = read_and_parse_s2s_config(filename)

            SetupVM.import_s2s(js)
        except Exception as e:
            flash(e, "danger")
            return redirect(url_for('url.setup_s2s'))

        flash("File uploaded successfully!", "success")
        return redirect(url_for('url.setup_s2s'))


@url.route("/export_s2s", methods=["GET"])
@login_required
def export_s2s():
    config = S2SConfig.query.get_or_404(1)
    logging.info("Exporting S2S config as JSON")

    # Write the config as JSON to file and then serve it as a download
    with tempfile.NamedTemporaryFile(mode="w+") as f:
        # Write to temporary file
        f.write(str(config))

        # Flush it, to ensure that the content is really written
        f.flush()

        # Read and serve the file as download
        return send_file(f.name, attachment_filename="s2s.config", as_attachment=True, mimetype="text/plain")


@url.route("/export_ra/<int:user_id>", methods=["POST"])
@login_required
def export_ra(user_id):
    image_path = SetupRPI.prepare_image(user_id)
    logging.info("Downloading SD-Card image...")
    return send_file(image_path, as_attachment=True)


@url.route("/ra", methods=["GET", "POST"])
@login_required
def ra():
    return render_template("ra_form.html")


@url.route("/s2s", methods=["GET", "POST"])
@login_required
def s2s():
    return render_template("s2s_form.html")


@url.route("/current_config", methods=["GET", "POST"])
@login_required
def current_config():
    return render_template("current_config_form.html")


@url.route("/ra_server", methods=["GET", "POST"])
@login_required
def setup_ra():
    # Form
    form_ra = RemoteAccessForm()

    # Form was submitted
    if form_ra.validate_on_submit():
        # Insert/Update to DB.
        RAConfig().update_db_ra(form_ra)

        SetupVM.setup_ra()

        flash("Config was saved and applied to the system!", "success")
        return redirect(url_for("url.current_config_ra"))
    return render_template("ra_server_form.html",
                           form_ra=form_ra,
                           ra_config=RAConfig.query.get(1),
                           title='Remote Access Setup')


@url.route("/user", methods=["GET", "POST"])
@login_required
def setup_user():
    # Form
    form_user = UserForm()

    # RA Config
    ra_config = RAConfig.query.get(1)

    # Form was submitted
    if form_user.validate_on_submit():
        user = UserModel()
        user_id = user.setup_update_db(form_user)
        SetupVM.setup_ra(user_id)

        # Flash a success alert
        flash("User added successfully!", "success")
        return redirect(url_for("url.setup_user"))
    return render_template("ra_user_form.html", form=form_user, ra_config=ra_config, title='User Setup')


@url.route("/edit/user/<int:user_id>", methods=["GET", "POST"])
@login_required
def edit_user(user_id):
    # Form
    form_user = UserForm()
    user = UserModel.query.get(user_id)

    # RA Config
    ra_config = RAConfig.query.get(1)

    # Form was submitted
    if form_user.validate_on_submit():
        user.edit_update_db(form_user)
        SetupVM.setup_ra(user_id)

        # Flash a success alert
        flash("User updated successfully!", "success")
        return redirect(url_for("url.current_config_ra"))
    return render_template("ra_user_form.html", form=form_user, ra_config=ra_config, user=user, title='Edit User')


@url.route("/delete/user/<int:user_id>", methods=["POST"])
@login_required
def delete_user(user_id):
    delete(UserModel, user_id)
    SetupVM.setup_ra(user_id)
    return redirect(url_for("url.current_config_ra"))


@url.route("/qrcode/user/<int:user_id>", methods=["GET"])
@login_required
def qrcode_user(user_id):
    # Generate QR Code
    SetupPhone.generate_qr_code(user_id)

    # Get current RA Config. Only show the WG QR Code if vpn-type if L3
    ra_config = RAConfig.query.get(1)

    return render_template('qrcode.html', ra_config=ra_config)


@url.route("/delete/ra/<int:ra_id>", methods=["POST"])
@login_required
def delete_ra(ra_id):
    # get the confg before deleting it
    ra_config = RAConfig.query.get(ra_id)
    # delete the iptables rule that was added for this config
    Iptables.delete_rule_wg(ra_config.a_public_port)

    # delete the config
    delete(RAConfig, ra_id)
    SetupVM.setup_ra()
    return redirect(url_for("url.current_config_ra"))


@url.route("/delete/s2s/<int:s2s_id>", methods=["POST"])
@login_required
def delete_s2s(s2s_id):
    # get the confg before deleting it
    s2s_config = S2SConfig.query.get(s2s_id)
    # delete the iptables rule that was added for this config
    Iptables.delete_rule_wg(s2s_config.a_public_port)

    # delete the config
    delete(S2SConfig, s2s_id)
    SetupVM.setup_s2s()
    return redirect(url_for("url.current_config_s2s"))


@url.route("/current_config_ra")
@login_required
def current_config_ra():
    ra_config = RAConfig.query.all()
    user = UserModel.query.all()
    return render_template('current_config_ra.html',
                           ra_config=ra_config,
                           user=user,
                           title='Current RA Config',
                           wg_state=WgQuick.get_state("wg1")
                           )


@url.route("/current_config_s2s")
@login_required
def current_config_s2s():
    s2s_config = S2SConfig.query.all()
    return render_template('current_config_s2s.html',
                           s2s_config=s2s_config,
                           title='Current S2S Config',
                           wg_state=WgQuick.get_state("wg0")
                           )


@url.route("/start_stop/<string:what>/<string:value>", methods=["POST"])
@login_required
def start_stop(what, value):
    if what == "s2s" and value == "start":
        WgQuick.up("wg0")
    elif what == "s2s" and value == "stop":
        WgQuick.down("wg0")
    elif what == "ra" and value == "start":
        WgQuick.up("wg1")
    elif what == "ra" and value == "stop":
        WgQuick.down("wg1")
    return redirect(url_for('url.status'))


@url.route("/system_logs")
@login_required
def system_logs():
    return render_template("system_logs.html", title='System Logs')


@url.route("/get_log/<string:log_type>", defaults={'lines': 100})
@url.route("/get_log/<string:log_type>/<int:lines>")
@login_required
def get_log(log_type, lines):
    # if lines over 100000000000000000000 (10²⁰) output fails
    if lines > 10000000000000000000:
        lines = 1000000
    return SystemLogs.get_log_file(log_type, lines)


@url.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        admin_user = LoginModel.query.get(1)
        username_bool = admin_user.username == form.username.data
        password_bool = LoginModel.check_password(admin_user, form.password.data)
        if username_bool and password_bool:
            flash('You have been logged in!', 'success')
            login_user(admin_user, remember=form.remember.data)
            return redirect(url_for('url.home'))
        else:
            flash('Wrong username or password provided!', 'danger')
    return render_template("login_form.html", form=form, title='Login')


@url.route("/logout")
@login_required
def logout():
    logout_user()
    flash('You have been logged out!', 'success')
    return redirect(url_for('url.home'))


@url.route("/changepw", methods=["GET", "POST"])
@login_required
def changepw():
    form = PasswordForm()
    if form.validate_on_submit():
        admin_user = LoginModel.query.get(1)
        check_old_password = LoginModel.check_password(admin_user, form.old_password.data)
        if not check_old_password:
            flash('Old provided password was wrong!', 'danger')
        check_new_password = form.new_password.data == form.confirm_password.data
        if not check_new_password:
            flash('New and confirm password do not match!', 'danger')
        if check_old_password and check_new_password:
            LoginModel.set_password(admin_user, form.confirm_password.data)
            flash('Password has been changed successfully', 'success')
    return render_template("changepw.html", form=form, title='Change Password')


@login_manager.user_loader
def load_user(user_id):
    """Check if user is logged-in on every page load."""
    if user_id is not None:
        return LoginModel.query.get(user_id)
    return None


@login_manager.unauthorized_handler
def unauthorized():
    """Redirect unauthorized users to Login page."""
    flash('You must be logged in to view that page.', 'info')
    return redirect(url_for('url.login'))
