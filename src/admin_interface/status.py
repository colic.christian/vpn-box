import logging
import re
from datetime import datetime

import humanize

from models import UserModel
from util import run_cmd, GeoIP


class OvsStatus:
    """
    Simple helper class to get the current OVS status
    """

    def __init__(self, ovs_output):
        self._ovs_output = ovs_output

    def get_output(self):
        return self._ovs_output

    def __repr__(self):
        return self._ovs_output


class WgStatus:
    """
    Helper class to read and parse the current Wireguard status
    Check if interfaces are configured and parse the 'wg show all dump' output
    """

    def __init__(self, wg_output):
        self.regexp_match_interface = r'^(wg\d)\t(\S+)\t(\S+)\t(\d+)\t(\w+)$'
        self.regexp_match_peer = r'^(wg\d)\t(\S+)\t(\S+)\t(\S+)\t(\S+)\t(\d+)\t(\d+)\t(\d+)\t(\w+)$'
        self._wg_output = wg_output
        self.peers_wg0 = list()
        self.peers_wg1 = list()
        self.interface = dict()
        self.parse_wg_dump()

    def is_configured_wg0(self, interface_number):
        if self.peers_wg0:
            return True
        else:
            return False

    def is_configured_wg1(self, interface_number):
        if self.peers_wg1:
            return True
        else:
            return False

    def get_interface(self, interface_number):
        return self.interface[interface_number]

    def get_peers_wg0(self):
        return self.peers_wg0

    def get_peers_wg1(self):
        return self.peers_wg1

    def parse_wg_dump(self):
        logging.info("Parsing Wireguard output dump")
        lines = self._wg_output.splitlines()
        for line in lines:
            match_interface = re.match(self.regexp_match_interface, line)
            if match_interface:
                interface_number = match_interface.group(1)
                self.parse_wg_interface(interface_number, match_interface)
            match_peer = re.match(self.regexp_match_peer, line)
            if match_peer:
                peer_number = match_peer.group(1)
                self.parse_wg_peer(peer_number, match_peer)

    def parse_wg_interface(self, interface_number, re_match):
        self.interface[interface_number] = dict()
        self.interface[interface_number]["interface"] = re_match.group(1)
        self.interface[interface_number]["private_key"] = re_match.group(2)
        self.interface[interface_number]["public_key"] = re_match.group(3)
        self.interface[interface_number]["listen_port"] = re_match.group(4)
        self.interface[interface_number]["fwmark"] = re_match.group(5)

    def parse_wg_peer(self, peer_number, re_match):
        peer = dict()
        if peer_number == 'wg0':
            peer["interface"] = re_match.group(1)
            peer["public_key"] = re_match.group(2)
            peer["pre_shared_key"] = re_match.group(3)
            peer["endpoint"] = re_match.group(4)
            peer["endpoint_ip"] = peer["endpoint"].split(':')[0]
            with GeoIP() as geo:
                peer["endpoint_geo_iso"] = geo.lookup_country_iso_code(peer["endpoint_ip"])
            peer["allowed_ips"] = re_match.group(5)
            latest_handshake = re_match.group(6)
            peer["latest_handshake"] = latest_handshake
            peer["latest_handshake_sec"] = int(
                (datetime.now() - datetime.fromtimestamp(int(latest_handshake))).total_seconds())
            peer["transfer_rx"] = humanize.naturalsize(re_match.group(7), binary=True)
            peer["transfer_tx"] = humanize.naturalsize(re_match.group(8), binary=True)
            peer["persistent_keepalive"] = re_match.group(9)
            self.peers_wg0.append(peer)
        elif peer_number == 'wg1':
            peer["interface"] = re_match.group(1)
            peer["public_key"] = re_match.group(2)
            peer["pre_shared_key"] = re_match.group(3)
            peer["endpoint"] = re_match.group(4)
            peer["endpoint_ip"] = peer["endpoint"].split(':')[0]
            with GeoIP() as geo:
                peer["endpoint_geo_iso"] = geo.lookup_country_iso_code(peer["endpoint_ip"])
            peer["allowed_ips"] = re_match.group(5)
            latest_handshake = re_match.group(6)
            peer["latest_handshake"] = latest_handshake
            peer["latest_handshake_sec"] = int(
                (datetime.now() - datetime.fromtimestamp(int(latest_handshake))).total_seconds())
            peer["transfer_rx"] = humanize.naturalsize(re_match.group(7), binary=True)
            peer["transfer_tx"] = humanize.naturalsize(re_match.group(8), binary=True)
            peer["persistent_keepalive"] = re_match.group(9)

            # Get the peers username
            user = UserModel.query.filter(UserModel.public_key == peer["public_key"]).first()
            peer["username"] = user.username

            self.peers_wg1.append(peer)


def wg_status():
    cp = run_cmd("sudo wg show all dump")
    return WgStatus(cp.stdout)


def ovs_status():
    try:
        cp = run_cmd("sudo ovs-vsctl show")
        return OvsStatus(cp.stdout)
    except RuntimeError as e:
        return "Can't communicate with Open vSwitch. Error: {}".format(e)
