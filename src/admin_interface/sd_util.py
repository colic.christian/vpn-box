import logging
import os.path
import re
import tempfile

from util import run_cmd


class SdUtil:
    """
    Utility class for all interactions with the SD card image
    Mounting, unmounting. Changing files, creating symlinks.
    Enabling or disabling of systemd services on the image, etc.
    """

    def __init__(self, img_path="/opt/vpn-box/sd.img", mount_path="/mnt/sd"):
        self.mount_path = mount_path
        if os.path.exists(img_path):
            self.img_path = img_path
        else:
            logging.error("SD card image at '{}' does not exist. Please copy it there.".format(self.img_path))
            raise FileExistsError

        if not os.path.exists(mount_path):
            run_cmd("sudo mkdir /mnt/sd")

    def mount(self):
        """
        Mount the SD card image to self.mount_path
        """
        # check if already mounted
        if not os.path.ismount(self.mount_path):
            offsets = self.get_offset()
            cp = run_cmd(
                "sudo mount -o loop,offset={} {} {}".format(512 * offsets["rootfs"], self.img_path, self.mount_path))
            if cp.returncode != 0:
                raise RuntimeError
            else:
                logging.info("SD card image successfully mounted at '{}'. Offset for rootfs was: {}".format(
                    self.mount_path, offsets["rootfs"]))
        else:
            logging.info("SD card image is already mounted at '{}'".format(self.mount_path))

    def umount(self):
        """
        Unmount the SD card image from self.mount_path
        """
        # check if already mounted
        if os.path.ismount(self.mount_path):
            cp = run_cmd("sudo umount {}".format(self.mount_path))
            if cp.returncode != 0:
                raise RuntimeError
            else:
                logging.info("SD card image successfully unmounted from '{}'".format(self.mount_path))
        else:
            logging.info("SD card image is not mounted at '{}'".format(self.mount_path))

    def get_offset(self):
        """
        Get the byte offsets of the partitions in the SD card image
        :return: {"boot": offset, "rootfs": offset}
        """
        cp = run_cmd("fdisk -l {}".format(self.img_path))
        lines = cp.stdout.splitlines()

        # Get the index of the header line. After this we will have a line for each partition, which contains the offset
        idx = int([i for i, line in enumerate(lines) if re.match(r'^\s*Device\s+Boot\s+Start', line)][0])
        boot_offset_line = lines[idx + 1]
        rootfs_offset_line = lines[idx + 2]
        match_boot = re.match(r'^\S+\s+(\d+)', boot_offset_line)
        match_rootfs = re.match(r'^\S+\s+(\d+)', rootfs_offset_line)
        boot_offset = int(match_boot.group(1))
        rootfs_offset = int(match_rootfs.group(1))
        logging.info("Got the SD card image partition offsets. boot partition offset: {}, rootfs partition offset: {}"
                     .format(boot_offset, rootfs_offset))
        return {"boot": boot_offset, "rootfs": rootfs_offset}

    def patch_file(self, path, content):
        """
        Patch a file in the mounted SD card image
        :param path: The absolute path to the file. The mount path will be added automatically
        :param content: Content of the file
        """
        logging.info("Patching file '{}' on SD card image".format(path))
        # Write the rendered jinja template to a temporary file and then copy it to the SD card image
        with tempfile.TemporaryDirectory() as temp_dir:
            with open(os.path.join(temp_dir, 'tmp.txt'), 'a') as tmp_file:
                tmp_file.write(content)
            run_cmd("sudo cp {} {}{}".format(os.path.join(temp_dir, 'tmp.txt'), self.mount_path, path))

    def delete_file(self, path):
        """
        Delete a file in the mounted SD card image
        :param path: The absolute path to the file. The mount path will be added automatically
        """
        logging.info("Deleting file '{}' on SD card image".format(path))
        run_cmd("sudo rm -f {}{}".format(self.mount_path, path))

    def create_symlink(self, src, dst):
        self.mount()
        logging.info("Creating symlink on SD card image: {} -> {}".format(dst, src))
        try:
            run_cmd("sudo ln -r -s {} {}".format(self.mount_path + src, self.mount_path + dst))
        except RuntimeError as e:
            if "File exists" in str(e):
                logging.info("Symlink already exists")

    def enable_hostapd(self, vpn_variant):
        if vpn_variant == "l2":
            # systemctl enable hostapd@l2
            logging.info("Enabling service hostapd@l2 on SD card image")
            self.create_symlink("/lib/systemd/system/hostapd@.service",
                                "/etc/systemd/system/multi-user.target.wants/hostapd@l2.service")
            self.create_symlink("/lib/systemd/system/hostapd@.service",
                                "/etc/systemd/system/sys-subsystem-net-devices-l2.device.wants/hostapd@l2.service")
            self.delete_file("/etc/systemd/system/sys-subsystem-net-devices-l3.device.wants/hostapd@l3.service")
            self.delete_file("/etc/systemd/system/multi-user.target.wants/hostapd@l3.service")
        if vpn_variant == "l3":
            # systemctl enable hostapd@l3
            logging.info("Enabling service hostapd@l3 on SD card image")
            self.create_symlink("/lib/systemd/system/hostapd@.service",
                                "/etc/systemd/system/multi-user.target.wants/hostapd@l3.service")
            self.create_symlink("/lib/systemd/system/hostapd@.service",
                                "/etc/systemd/system/sys-subsystem-net-devices-l3.device.wants/hostapd@l3.service")
            self.delete_file("/etc/systemd/system/sys-subsystem-net-devices-l2.device.wants/hostapd@l2.service")
            self.delete_file("/etc/systemd/system/multi-user.target.wants/hostapd@l2.service")

    def enable_dnsmasq(self):
        # systemctl enable dnsmasq
        logging.info("Enabling service dnsmasq on SD card image")
        self.create_symlink("/lib/systemd/system/dnsmasq.service",
                            "/etc/systemd/system/multi-user.target.wants/dnsmasq.service")

    def disable_dnsmasq(self):
        # systemctl disable dnsmasq
        logging.info("Disabling service dnsmasq on SD card image")
        self.delete_file("/etc/systemd/system/multi-user.target.wants/dnsmasq.service")
