import logging
import os
from logging.handlers import RotatingFileHandler

from flask import Flask, render_template
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from sqlalchemy import text

from config import Config
from db import db
from util import inject_global_variable, get_git_revision_short_hash

# Logging
log_level = logging.INFO
log_format = "%(asctime)s [%(levelname)-5.5s] [%(filename)s: %(funcName)-15s() ]:  %(message)s"

# set up logging to console
consoleHandler = logging.StreamHandler()
consoleHandler.setLevel(log_level)
formatter = logging.Formatter(log_format)
consoleHandler.setFormatter(formatter)

# setup logging to file
fileHandler = RotatingFileHandler('app.log', maxBytes=10000, backupCount=1)
fileHandler.setLevel(log_level)

logging.basicConfig(
    level=log_level,
    format=log_format,
    handlers=[fileHandler, consoleHandler]
)

# Define the WSGI application object
app = Flask(__name__)

# inject the current git_revision as a global variable into the app context
git_rev = dict(git_revision=get_git_revision_short_hash())
inject_global_variable(app, git_rev)

# Define instance to hash und check hash of passwords
bcrypt = Bcrypt()

# Create Login Manager to handle user logins
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login.html'

from controller import url

# Configuration
app.config.from_object(Config)
if os.getenv('ENV') != 'dev':
    app.config['DEBUG'] = False

# Define the database object which is imported
# by modules and controllers
db.init_app(app)


# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


# Register blueprint(s)
app.register_blueprint(url)

# Build the database:
# This will create the database file using SQLAlchemy
with app.app_context():
    db.create_all()
    # add admin user to login
    result = db.session.execute(text('SELECT COUNT(*) FROM login'))
    count = [row[0] for row in result][0]
    if count == 0:
        db.session.execute(text(
            "INSERT INTO login VALUES (1, 'admin', '$2b$12$CWvTmFE97ovYycv6.69keOJlmsa1pouw3zr7acLWWjowDMpeVdHfO', 0)"
        ).execution_options(autocommit=True))  # autocommits
        db.session.commit()
