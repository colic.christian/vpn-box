import logging
import re

from flask_wtf import FlaskForm
from flask_wtf.file import FileRequired
from wtforms import StringField, SubmitField, IntegerField, BooleanField, FileField
from wtforms.validators import NumberRange, ValidationError, InputRequired
from ipaddress import ip_network

from models import S2SConfig, RAConfig, UserModel

# Validate IP (IPv4 or IPv6)
regexp_ip = (r'((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4]['
             '0-9]|25[0-5]))\s*$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:['
             '0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|((['
             '0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2['
             '0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,'
             '4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,'
             '4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25['
             '0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,'
             '5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){'
             '3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2['
             '0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,'
             '7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){'
             '3}))|:)))(%.+)?\s*$))')

# Validate IPv6 in prefix (CIDR) format
regexp_ipv6_cidr = (
    r'^s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2['
    r'0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,'
    r'2})|:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:['
    r'0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){'
    r'3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]d|1dd|['
    r'1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,'
    r'5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|((['
    r'0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25['
    r'0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2['
    r'0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:)))(%.+)?s*(\/([0-9]|[1-9][0-9]|1[0-1][0-9]|12['
    r'0-8]))?$')

# Validate IPv4 netmask
regex_ipv4_netmask = (
    "^(((255\.){3}(255|254|252|248|240|224|192|128|0+))|((255\.){2}("
    "255|254|252|248|240|224|192|128|0+)\.0)|((255\.)(255|254|252|248|240|224|192|128|0+)(\.0+){"
    "2})|((255|254|252|248|240|224|192|128|0+)(\.0+){3}))$")

# Validate IPv4 in prefix (CIDR) format
regexp_ipv4_cidr = r'^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2]))?$'


def check_ip(form, field):
    """
    Custom validator for IPs
    :param form: The form
    :param field: The field to be validated
    :return: raises a ValidationError on error
    """
    if not re.match(regexp_ip, field.data):
        logging.error("IP '{}' failed validation!".format(field.data))
        raise ValidationError("Invalid IP")


def check_ip_or_emptystring(form, field):
    """
    Custom validator for IPs
    :param form: The form
    :param field: The field to be validated
    :return: raises a ValidationError on error
    """
    if not re.match(regexp_ip, field.data) and field.data != '':
        logging.error("IP '{}' failed validation!".format(field.data))
        raise ValidationError("Invalid IP")


def check_ip_cidr(form, field):
    """
    Custom validator for IPv4 and IPv6 in CIDR format
    :param form: The form
    :param field: The field to be validated
    :return: raises a ValidationError on error
    """
    if not re.match(regexp_ipv4_cidr, field.data) and not re.match(regexp_ipv6_cidr, field.data):
        logging.error("IP CIDR '{}' failed validation!".format(field.data))
        raise ValidationError("Invalid IP CIDR")


def check_ip_cidr_wg0(form, field):
    """
    Custom validator for IPv4 and IPv6 in CIDR format for overlapping Site-To-Site Wireguard ip addressing
    :param form: The form
    :param field: The field to be validated
    :return: raises a ValidationError on error
    """
    if not re.match(regexp_ipv4_cidr, field.data) and not re.match(regexp_ipv6_cidr, field.data):
        logging.error("IP CIDR '{}' failed validation!".format(field.data))
        raise ValidationError("Invalid IP CIDR")

    config = RAConfig.query.get(1)
    if config is not None:
        ra_net = ip_network(config.a_tunnel_cidr, strict=False)
        given_net = ip_network(field.data, strict=False)

        if ra_net.overlaps(given_net):
            logging.error("Overlapping IP CIDR '{}' - failed validation!".format(field.data))
            raise ValidationError("This IP CIDR is overlapping with Remote-Access VPN!")


def check_ip_cidr_wg1(form, field):
    """
    Custom validator for IPv4 and IPv6 in CIDR format for overlapping Remote-Access Wireguard ip addressing
    :param form: The form
    :param field: The field to be validated
    :return: raises a ValidationError on error
    """
    if not re.match(regexp_ipv4_cidr, field.data) and not re.match(regexp_ipv6_cidr, field.data):
        logging.error("IP CIDR '{}' failed validation!".format(field.data))
        raise ValidationError("Invalid IP CIDR")

    config = S2SConfig.query.get(1)
    if config is not None:
        s2s_net = ip_network(config.a_tunnel_cidr, strict=False)
        given_net = ip_network(field.data, strict=False)

        if s2s_net.overlaps(given_net):
            logging.error("Overlapping IP CIDR '{}' - failed validation!".format(field.data))
            raise ValidationError("This IP CIDR is overlapping with Site-To-Site VPN!")


def check_ip_cidr_user(form, field):
    """
    Custom validator for IPv4 / IPv6 in CIDR format and to check whether this ip cidr is in the range of RA Tunnel CIDR
    :param form: The form
    :param field: The field to be validated
    :return: raises a ValidationError on error
    """
    if not re.match(regexp_ipv4_cidr, field.data) and not re.match(regexp_ipv6_cidr, field.data):
        logging.error("IP CIDR '{}' failed validation!".format(field.data))
        raise ValidationError("Invalid IP CIDR")

    config = RAConfig.query.get(1)
    if config is not None:
        s2s_net = ip_network(config.a_tunnel_cidr, strict=False)
        given_net = ip_network(field.data, strict=False)

        if not s2s_net == given_net:
            logging.error("IP CIDR '{}' not in range of RA Tunnel CIDR IP - failed validation!".format(field.data))
            raise ValidationError("This Tunnel IP is not within the range of the configured RA Tunnel IP Range!")


def check_ip_cidr_or_emptystring(form, field):
    """
    Custom validator for IPs
    :param form: The form
    :param field: The field to be validated
    :return: raises a ValidationError on error
    """
    if field.data == '':
        return
    if not re.match(regexp_ipv4_cidr, field.data) and not re.match(regexp_ipv6_cidr, field.data):
        logging.error("IP '{}' failed validation!".format(field.data))
        raise ValidationError("Invalid IP")


def check_netmask(form, field):
    """
    Custom validator for IPv4 subnet mask
    :param form: The form
    :param field: The field to be validated
    :return: raises a ValidationError on error
    """
    if not re.match(regex_ipv4_netmask, field.data):
        logging.error("Subnet mask '{}' failed validation!".format(field.data))
        raise ValidationError("Invalid Subnet mask")


def check_local_networks(form, field):
    """
    Custom validator for a list of local networks in CIDR format
    :param form: The form
    :param field: The field to be validated
    :return: raises a ValidationError on error
    """

    # don't validate these fields if L3-Status checkbox was unchecked
    if not form.l3.data:
        return
    networks = [x.strip() for x in field.data.split(",")]
    for net in networks:
        if not re.match(regexp_ipv4_cidr, net) and not re.match(regexp_ipv6_cidr, net):
            logging.error("Local network IP CIDR '{}' failed validation!".format(net))
            raise ValidationError("Invalid local network")


def check_changepw(form, field):
    """
    Custom validator for Passwords
    :param form: The form
    :param field: The field to be validated
    :return: raises a ValidationError on error
    """
    if len(field.data) < 8:
        raise ValidationError("Password must at least contain 8 characters")


def check_s2s_port(form, field):
    check_listen_port(field.data)
    ra_config = RAConfig.query.get(1)
    if ra_config is not None:
        ra_port = ra_config.a_public_port
        if field.data == ra_port:
            raise ValidationError("This port is already in use for Remote-Access VPN!")


def check_ra_port(form, field):
    check_listen_port(field.data)
    s2s_config = S2SConfig.query.get(1)
    if s2s_config is not None:
        s2s_port = s2s_config.a_public_port
        if field.data == s2s_port:
            raise ValidationError("This port is already in use for Site-to-Site VPN!")


def check_listen_port(port):
    if port < 1 or port > 65535:
        raise ValidationError("Port number not in range (1-65535)")


class SiteToSiteSetupForm(FlaskForm):
    class Meta:
        model = S2SConfig
        exclude = ['time_created']

    a_public_ip = StringField("Public IP", validators=[check_ip])
    a_public_port = IntegerField("Public Port", validators=[
        NumberRange(min=1, max=65535, message="Port number not in range (1-65535)")])
    a_tunnel_cidr = StringField("Tunnel IP + Mask (CIDR)", validators=[check_ip_cidr_wg0])
    a_local_networks = StringField("Local Networks", validators=[check_local_networks])
    b_public_ip = StringField("Public IP", validators=[check_ip])
    b_public_port = IntegerField("Public Port", validators=[check_s2s_port])
    b_tunnel_cidr = StringField("Tunnel IP + Mask (CIDR)", validators=[check_ip_cidr])
    b_local_networks = StringField("Local Networks", validators=[check_local_networks])

    psk = BooleanField("Generate and use a pre-shared key")
    keepalive = IntegerField("Keepalive", validators=[
        NumberRange(min=0, max=65535, message="Keepalive in seconds in range (0-65535)")])
    l3 = BooleanField()
    submit = SubmitField('Upload new S2S config-parameters')


class SiteToSiteImportForm(FlaskForm):
    # with FileRequired() the upload can't be performed until having chosen a file
    config_file = FileField(validators=[FileRequired()])
    submit = SubmitField('Upload new S2S config-file')


class RemoteAccessForm(FlaskForm):
    class Meta:
        model = RAConfig
        exclude = ['time_created']

    a_public_ip = StringField("Public IP", validators=[check_ip])
    a_public_port = IntegerField("Public Port", validators=[check_ra_port])
    a_tunnel_cidr = StringField("Tunnel IP + Mask (CIDR)", validators=[check_ip_cidr_wg1])
    a_local_networks = StringField("Local Networks", validators=[check_local_networks])
    dns1 = StringField("DNS 1 IP", validators=[check_ip_or_emptystring])
    dns2 = StringField("DNS 2 IP", validators=[check_ip_or_emptystring])
    l3 = BooleanField()
    submit = SubmitField('Upload new RA config-parameters')


class UserForm(FlaskForm):
    class Meta:
        model = UserModel
        exclude = ['time_created']

    username = StringField("Username")
    firstname = StringField("Firstname")
    surname = StringField("Surname")
    tunnel_cidr = StringField("Tunnel IP + Mask (CIDR)", validators=[check_ip_cidr_user])
    public_key = StringField("Public Key")
    private_key = StringField("Private Key")
    use_psk = BooleanField("Generate and use a pre-shared key")
    keepalive = IntegerField("Keepalive", validators=[
        NumberRange(min=0, max=65535, message="Keepalive in seconds in range (0-65535)")])
    wifi_cidr = StringField("WiFi Router IP + Subnet Mask (CIDR)", validators=[check_ip_cidr_or_emptystring])
    submit = SubmitField('Create new User')


class StaticIpForm(FlaskForm):
    ipv4_addr = StringField("IPv4 address", validators=[check_ip])
    ipv4_netmask = StringField("IPv4 Subnet mask", validators=[check_netmask])
    ipv4_gateway = StringField("IPv4 Gateway", validators=[check_ip])
    ipv6_addr = StringField("IPv6 address", validators=[check_ip_cidr_or_emptystring])
    ipv6_gateway = StringField("IPv6 Gateway", validators=[check_ip_or_emptystring])
    dns1 = StringField("DNS 1", validators=[check_ip_or_emptystring])
    dns2 = StringField("DNS 2", validators=[check_ip_or_emptystring])
    submit = SubmitField('Submit')


class LoginForm(FlaskForm):
    username = StringField("Username")
    password = StringField("Password")
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')


class PasswordForm(FlaskForm):
    old_password = StringField("Old Password", validators=[InputRequired()])
    new_password = StringField("New Password", validators=[check_changepw])
    confirm_password = StringField("Confirm Password", validators=[check_changepw])
    submit = SubmitField('Change Password')
