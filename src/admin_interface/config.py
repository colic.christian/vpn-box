import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    # Statement for enabling the development environment
    DEBUG = True

    # Define the application directory
    BASE_DIR = basedir

    # Define the database - we are working with
    # SQLite for this example
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DATABASE_CONNECT_OPTIONS = {}

    # Application threads. A common general assumption is
    # using 2 per available processor cores - to handle
    # incoming requests using one and performing background
    # operations using the other.
    THREADS_PER_PAGE = 2

    # Enable protection against *Cross-site Request Forgery (CSRF)*
    CSRF_ENABLED = True

    # Use a secure, unique and absolutely secret key for
    # signing the data.
    CSRF_SESSION_KEY = "rrRzSY3UyUdPosqZG4EfgaJ2"

    # Secret key for signing cookies
    SECRET_KEY = "Q68HqGCVfThUgSjUXM13FFFb"

    BOOTSTRAP_SERVE_LOCAL = True
    JSONIFY_PRETTYPRINT_REGULAR = True

    # Set max file age to zero - relating browser caching.
    SEND_FILE_MAX_AGE_DEFAULT = 0
