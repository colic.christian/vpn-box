import logging

from util import run_cmd


class OVS:
    """
    Helper class for interaction with OVS
    Adding and removing VXLAN ports
    """

    VXLAN_MAX_KEY = 16777215

    @staticmethod
    def add_vxlan_port(remote_ip, key):
        logging.info("Adding OVS VXLAN port with remote-ip '{}' and VXLAN-ID '{}'".format(remote_ip, key))
        cp = run_cmd("sudo ovs-vsctl list-ports br-internal")
        interface_name = "vx{}".format(key)
        for line in cp.stdout.splitlines():
            if line == interface_name:
                # if it already exists, remove it first
                logging.info("OVS Port '{}' already exists. Removing it first.".format(interface_name))
                run_cmd("sudo ovs-vsctl del-port br-internal {}".format(interface_name))

        # add new vxlan port to OVS bridge br-internal
        logging.info("Add new OVS VXLAN Port '{}'".format(interface_name))
        run_cmd("sudo ovs-vsctl add-port br-internal {interface_name} -- set interface {interface_name} type=vxlan "
                "options:remote_ip={remote_ip} options:key={key}".format(interface_name=interface_name,
                                                                         remote_ip=remote_ip, key=key))

    @staticmethod
    def del_vxlan_port(port):
        logging.info("Removing OVS Port '{}'".format(port))
        # list all OVS ports on bridge br-internal
        cp = run_cmd("sudo ovs-vsctl list-ports br-internal")
        interface_name = port
        if interface_name in cp.stdout:
            # if the port exists, remove from OVS (runtime)
            run_cmd("sudo ovs-vsctl del-port br-internal {}".format(interface_name))

        # also remove the ifcfg network config file if it exists
        run_cmd("sudo rm -f /etc/sysconfig/network-scripts/ifcfg-{}".format(port))

    @staticmethod
    def delete_all_s2s_vxlan_ports():
        logging.info("Deleting all S2S OVS VXLAN ports")
        cp = run_cmd("sudo ovs-vsctl list-ports br-internal")
        for line in cp.stdout.splitlines():
            if line.strip() == "vx16777215":
                OVS.del_vxlan_port(line)

    @staticmethod
    def delete_all_ra_vxlan_ports():
        logging.info("Deleting all RA OVS VXLAN ports")
        cp = run_cmd("sudo ovs-vsctl list-ports br-internal")
        for line in cp.stdout.splitlines():
            if line.strip().startswith("vx") and line.strip() != "vx16777215":
                OVS.del_vxlan_port(line)
