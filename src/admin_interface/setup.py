import logging
import re

import ipaddress
import netifaces as ni

from forms import SiteToSiteSetupForm
from models import RAConfig, UserModel, S2SConfig
from ovs import OVS
from sd_util import SdUtil
from util import jinja_env
from util import run_cmd, patch_file, create_wg_qr_code_image, Iptables, create_wifi_qr_code_image
from wg import WG_RA, WG_S2S, WG_User


class SetupPhone:
    """
    Helper class for setting up the Wireguard QR code configs
    """

    @staticmethod
    def generate_qr_code(user_id):
        wg_user = WG_User(user_id)
        # generate wireguard qr-code
        rendered = jinja_env.get_template('rpi/wg.conf.j2').render(wg_config=wg_user)
        create_wg_qr_code_image(rendered)
        # generate wifi qr-code
        ra_config = RAConfig.query.get(1)
        if ra_config.l3:
            create_wifi_qr_code_image('WiFi-VPN L3', wg_user.wifi)
        else:
            create_wifi_qr_code_image('WiFi-VPN L2', wg_user.wifi)


class SetupNetworking:
    """
    Helper class for getting and changing the VMs network settings
    """

    @staticmethod
    def get_sys_ip(interface):
        try:
            ipv4 = ni.ifaddresses(interface)[ni.AF_INET][0]
        except KeyError:
            ipv4 = {}
        try:
            ipv6 = ni.ifaddresses(interface)[ni.AF_INET6][0]
        except KeyError:
            ipv6 = {}
        ip = {"ipv4": ipv4, "ipv6": ipv6}
        return ip

    @staticmethod
    def get_sys_route():
        try:
            ipv4 = ni.gateways()['default'][ni.AF_INET][0]
        except KeyError:
            ipv4 = {}
        try:
            ipv6 = ni.gateways()['default'][ni.AF_INET6][0]
        except KeyError:
            ipv6 = {}
        gw = {"ipv4": ipv4, "ipv6": ipv6}
        return gw

    @staticmethod
    def get_sys_bootproto():
        cp = run_cmd("grep -E 'BOOTPROTO' /etc/sysconfig/network-scripts/ifcfg-br-wan")
        return cp.stdout.split('=')[1]

    @staticmethod
    def get_sys_dns():
        nameservers = []
        re_nameserver = r'nameserver\s+(.*)$'
        resolv = open("/etc/resolv.conf").read().splitlines()
        for line in resolv:
            match = re.match(re_nameserver, line)
            if match:
                nameservers.append(match.group(1))
        return nameservers

    @staticmethod
    def set_sys_network(form_data):
        logging.info("Changing system network settings")

        ipv4_addr = form_data['ipv4_addr']
        ipv4_netmask = form_data['ipv4_netmask']
        ipv4_gateway = form_data['ipv4_gateway']
        ipv6_addr = form_data['ipv6_addr']
        ipv6_gateway = form_data['ipv6_gateway']
        dns1 = form_data['dns1']
        dns2 = form_data['dns2']

        logging.info("IPv4 addr:'{ipv4_addr}'; IPv4 Mask:'{ipv4_netmask}'; IPv4 GW:'{ipv4_gateway}'; IPv6 addr:"
                     "'{ipv6_addr}'; IPv6 GW:'{ipv6_gateway}'; DNS1:'{dns1}'; DNS2:'{dns2}'".format(**locals()))
        rendered = jinja_env.get_template('system/ifcfg-br-wan.j2').render(
            ipv4_addr=ipv4_addr,
            ipv4_netmask=ipv4_netmask,
            ipv4_gateway=ipv4_gateway,
            ipv6_addr=ipv6_addr,
            ipv6_gateway=ipv6_gateway,
            dns1=dns1,
            dns2=dns2
        )
        patch_file("/etc/sysconfig/network-scripts/ifcfg-br-wan", rendered)
        run_cmd("sudo systemctl restart network")


class SetupRPI:
    """
    Helper class for settings up the RPI image
    Execute various actions depending on which VPN (S2S or RA) was changed
    and which type it is (L2 or L3)
    """

    @staticmethod
    def prepare_image(user_id):
        # mount image
        sd = SdUtil()
        sd.mount()

        # prepare the image depending on vpn-type L3 or L2
        ra_config = RAConfig.query.get(1)
        if ra_config.l3:
            SetupRPI.prepare_l3_image(sd, user_id)
        if not ra_config.l3:
            SetupRPI.prepare_l2_image(sd, user_id)

        return sd.img_path

    @staticmethod
    def prepare_l3_image(sd, user_id):
        logging.info("Preparing RaspberryPI L3 Image")

        # write wireguard specific config (wg-interfaces)
        logging.info("Writing Wireguard config to /etc/wireguard/wg1.conf")
        wg_user = WG_User(user_id)
        rendered = jinja_env.get_template('rpi/wg.conf.j2').render(wg_config=wg_user)
        sd.patch_file("/etc/wireguard/wg1.conf", rendered)

        ### wifi subnet and dhcp range
        wifi_net = ipaddress.ip_interface(wg_user.wifi_cidr).network
        # the router ip: the first IP of the network, with slash (cidr) notation
        router_ip = list(wifi_net.hosts())[0].compressed
        router_ip_cidr = list(wifi_net.hosts())[0].compressed + "/" + str(wifi_net.prefixlen)
        # dhcp range starts at the second usable host address (the first one will be the gateway/router)
        dhcp_mask = wifi_net.netmask.compressed
        dhcp_start = list(wifi_net.hosts())[1].compressed
        dhcp_end = list(wifi_net.hosts())[-1].compressed

        # check whether dns_ips is an ipv6 ip-address
        if ":" not in wg_user.dns_ips:
            # write dnsmasq.conf and modify dns if specified
            rendered = jinja_env.get_template('rpi/dnsmasq.conf.j2').render(
                wg_config=wg_user, router_ip=router_ip, dhcp_mask=dhcp_mask, dhcp_start=dhcp_start, dhcp_end=dhcp_end)
            logging.info("Writing dnsmasq config")
            sd.patch_file("/etc/dnsmasq.conf", rendered)

        # write raspberry specific config (wifi-password)
        logging.info("Writing L3 Hostapd config")
        user = UserModel.query.get(user_id)
        rendered = jinja_env.get_template('rpi/hostapd/l3.conf.j2').render(
            wifi_password=user.wifi_password)
        sd.patch_file("/etc/hostapd/l3.conf", rendered)

        # delete OpenvSwitch specific config (vxlan interface)
        logging.info("Removing OVS VXLAN port vx1 from network config (ifcfg-vx1)")
        sd.delete_file("/etc/network/interfaces.d/vx1")

        # write iptables config
        logging.info("Writing L3 iptables config")
        rendered = jinja_env.get_template('rpi/rules.v4').render(
            l3=True, wifi_net=wifi_net.compressed)
        sd.patch_file("/etc/iptables/rules.v4", rendered)

        # write BR-INTERNAL network interfaces file with static IP
        logging.info("Writing ifcfg-br-internal with static IP")
        rendered = jinja_env.get_template('rpi/br-internal.j2').render(
            l3=True, router_ip_cidr=router_ip_cidr
        )
        sd.patch_file("/etc/network/interfaces.d/br-internal", rendered)

        logging.info("Enabling dnsmasq and hostapd@l3 services")
        sd.enable_dnsmasq()
        sd.enable_hostapd("l3")

        # unmount
        sd.umount()

    @staticmethod
    def prepare_l2_image(sd, user_id):
        logging.info("Preparing RaspberryPI L2 Image")

        # write wireguard specific config (wg-interfaces)
        logging.info("Writing Wireguard config to /etc/wireguard/wg1.conf")
        wg_user = WG_User(user_id)
        rendered = jinja_env.get_template('rpi/wg.conf.j2').render(wg_config=wg_user)
        sd.patch_file("/etc/wireguard/wg1.conf", rendered)

        # check whether dns_ips is an ipv6 ip-address
        if ":" not in wg_user.dns_ips:
            # write dnsmasq.conf and modify dns if specified
            rendered = jinja_env.get_template('rpi/dnsmasq.conf.j2').render(
                wg_config=wg_user)
            logging.info("Writing dnsmasq config")
            sd.patch_file("/etc/dnsmasq.conf", rendered)

        # write raspberry hostapd specific config (wifi-password)
        logging.info("Writing L2 Hostapd config")
        user = UserModel.query.get(user_id)
        rendered = jinja_env.get_template('rpi/hostapd/l2.conf.j2').render(
            wifi_password=user.wifi_password)
        sd.patch_file("/etc/hostapd/l2.conf", rendered)

        # write OpenvSwitch specific config (vxlan interface)
        # the VXLAN remote IP is the peers tunnel IP
        # the VXLAN key is the user's ID
        logging.info("Adding OVS VXLAN port vx1 to network config (ifcfg-vx1)")
        rendered = jinja_env.get_template('rpi/vx1.j2').render(
            vxlan_remote_ip=wg_user.peers[0].tunnel_ip,
            vxlan_key=user_id
        )
        sd.patch_file("/etc/network/interfaces.d/vx1", rendered)

        # write iptables config
        logging.info("Writing L2 iptables config")
        rendered = jinja_env.get_template('rpi/rules.v4').render(
            l3=False)
        sd.patch_file("/etc/iptables/rules.v4", rendered)

        # write BR-INTERNAL network interfaces file without static IP
        logging.info("Writing ifcfg-br-internal without static IP")
        rendered = jinja_env.get_template('rpi/br-internal.j2').render(
            l3=False, ip=False
        )
        sd.patch_file("/etc/network/interfaces.d/br-internal", rendered)

        logging.info("Disabling dnsmasq and enabling hostapd@l2 services")
        sd.disable_dnsmasq()
        sd.enable_hostapd("l2")

        # unmount
        sd.umount()


class SetupVM:
    """
    Helper class for settings up the VM
    Execute various actions depending on which VPN (S2S or RA) was changed
    and which type it is (L2 or L3)
    """

    @staticmethod
    def setup_s2s():
        logging.info("Setting up VM for S2S")

        # Create Wireguard config from DB data
        wg_s2s = WG_S2S()

        # Generate WG config and write to system
        wg_s2s.configure_system()

        # get the systems S2S Config, if its in L2 setup, then setup vxlan too
        s2s_config = S2SConfig.query.get(1)
        if s2s_config:
            if not Iptables.exists_rule_wg(wg_s2s.interface["public_port"]):
                Iptables.add_rule_wg(wg_s2s.interface["public_port"])

            if not s2s_config.l3:
                SetupVM.setup_l2_s2s()
            elif s2s_config.l3:
                logging.info("VPN-Type: L3. Remove existing VXLAN ports")
                OVS.delete_all_s2s_vxlan_ports()
        else:
            # if there is no more S2S config,because it was deleted: cleanup vxlan ports
            logging.info("No S2S config found. Remove existing VXLAN ports")
            OVS.delete_all_s2s_vxlan_ports()

    @staticmethod
    def setup_l2_s2s():
        logging.info("VPN-Type: L2. Add VXLAN ports")

        # get the VXLAN remote ip, which is the peers tunnel ip
        wg_s2s = WG_S2S()
        vxlan_remote_ip = wg_s2s.peers[0].tunnel_ip

        logging.info("VXLAN remote IP is '{}'".format(vxlan_remote_ip))

        # add vxlan port to OVS (runtime)
        OVS.add_vxlan_port(vxlan_remote_ip, OVS.VXLAN_MAX_KEY)

        # write the vxlan interface config to /etc/sysconfig/network-script for persistence
        logging.info("Writing ifcfg-vx{}".format(OVS.VXLAN_MAX_KEY))
        rendered = jinja_env.get_template('system/ifcfg-vx.j2').render(
            port_number=OVS.VXLAN_MAX_KEY,
            remote_ip=vxlan_remote_ip
        )
        patch_file("/etc/sysconfig/network-scripts/ifcfg-vx{}".format(OVS.VXLAN_MAX_KEY), rendered)

    @staticmethod
    def import_s2s(import_config):
        # Create Wireguard config from imported data
        wg_s2s = WG_S2S(import_config=import_config, form=SiteToSiteSetupForm())

        # Generate WG config and write to system
        wg_s2s.configure_system()

        if not Iptables.exists_rule_wg(wg_s2s.interface["public_port"]):
            Iptables.add_rule_wg(wg_s2s.interface["public_port"])

        # get the systems S2S Config, if its in L2 setup, then setup vxlan too
        s2s_config = S2SConfig.query.get(1)
        if not s2s_config.l3:
            SetupVM.setup_l2_s2s()
        elif s2s_config.l3:
            logging.info("VPN-Type: L3. Remove existing VXLAN ports")
            OVS.delete_all_s2s_vxlan_ports()

    @staticmethod
    def setup_ra(user_id=None):
        logging.info("Setting up VM for RA")

        # Create Wireguard config from DB data
        wg_ra = WG_RA()
        # Generate WG config and write to system
        wg_ra.configure_system()

        # get the systems RA_Config, if its in L2 setup, then setup vxlan too
        ra_config = RAConfig.query.get(1)

        # if there is no more RA config, because it was deleted: cleanup vxlan ports
        if not ra_config:
            logging.info("No RA config found. Remove existing VXLAN ports")
            OVS.delete_all_ra_vxlan_ports()
            return

        if not Iptables.exists_rule_wg(wg_ra.interface["public_port"]):
            Iptables.add_rule_wg(wg_ra.interface["public_port"])

        if not ra_config.l3:
            if user_id:
                # setup vxlan for this user
                logging.info("Added new user. Create VXLAN port vx{}".format(user_id))
                SetupVM.setup_ra_peer(user_id)
            elif not user_id:
                # setup vxlan for all users
                logging.info("Create VXLAN ports for all users")
                users = UserModel.query.all()
                for user in users:
                    SetupVM.setup_ra_peer(user.id)

        elif ra_config.l3:
            logging.info("VPN-Type: L3. Remove existing VXLAN ports")
            OVS.delete_all_ra_vxlan_ports()

    @staticmethod
    def setup_ra_peer(user_id):
        # setup vxlan
        logging.info("Setup VXLAN port for User-ID '{}'".format(user_id))
        wg_ra = WG_RA()
        vxlan_remote_ip = None
        for peer in wg_ra.peers:
            if peer.user_id == user_id:
                vxlan_remote_ip = peer.tunnel_ip
                break

        if vxlan_remote_ip is None:
            # if this peer does not exist anymore, delete the vxlan port
            logging.info("This peer does not exist anymore. Remove VXLAN port")
            OVS.del_vxlan_port("vx{}".format(user_id))
        else:
            logging.info("VXLAN Remote IP is '{}'".format(vxlan_remote_ip))
            OVS.add_vxlan_port(vxlan_remote_ip, user_id)
