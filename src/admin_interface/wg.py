import json

from util import jinja_env
from models import S2SConfig, RAConfig, UserModel
from util import patch_file, WgQuick, modify_endpoint


class WgPeer:
    """
    This represents a Wireguard peer with all needed options
    """

    def __init__(self, tunnel_cidr="", public_ip="", public_port="", public_key="", psk="", local_networks="",
                 keepalive=0, username="", firstname="", surname="", user_id=0):
        self.tunnel_cidr = tunnel_cidr
        self.tunnel_ip = tunnel_cidr.split('/')[0]
        self.tunnel_prefix = tunnel_cidr.split('/')[1]
        self.public_ip = public_ip
        self.public_port = public_port
        self.public_key = public_key
        self.psk = psk
        self.local_networks = local_networks
        ip_list = [self.tunnel_ip, self.local_networks]
        self.allowed_ips = ",".join(filter(None, ip_list))
        self.keepalive = keepalive
        self.user_id = user_id
        self.username = username
        self.firstname = firstname
        self.surname = surname

    def has_endpoint(self):
        # returns true if the peer has a set endpoint address (public ip)
        # this will be true if the peer is a site-to-site endpoint
        # this will be false if the peer is a roaming client
        return bool(self.public_ip)


class WG:
    """
    Helper class for excapsulating a Wireguard Config
    This is an abstract class that will have more specific implementations
    for S2S and RA.
    It handles writing the current config to the system.
    """

    def __init__(self, import_config=None, form=None):
        self.wg_interface = ""
        self.interface = {'address': "", "private_key": "", "public_port": ""}
        self.peers = []
        if import_config:
            self.import_config(import_config, form)
        else:
            self.get_from_db()

    def get_from_db(self):
        raise NotImplementedError("You should call this on the Subclasses WG_RA or WG_S2S!")

    def import_config(self, config, form):
        raise NotImplementedError("You should call this on the Subclasses WG_RA or WG_S2S!")

    def __repr__(self):
        return json.dumps(self)

    def configure_system(self):
        if self.wg_interface == "":
            raise NotImplementedError("You should call this on the Subclasses WG_RA or WG_S2S!")
        rendered = jinja_env.get_template('wg.conf.j2').render(wg=self)
        patch_file("/etc/wireguard/{}.conf".format(self.wg_interface), rendered)
        WgQuick.restart(self.wg_interface)


class WG_RA(WG):
    """
    Helper class. This represents a RemoteAccess Wireguard config
    and makes all options and settings available.
    It handles loading and parsing of the current config from the database.
    """

    def __init__(self, import_config=None, form=None):
        super().__init__(import_config=import_config, form=form)
        self.wg_interface = "wg1"

    def get_from_db(self):
        current_ra_config = RAConfig.query.get(1)
        if current_ra_config:
            self.interface['private_key'] = current_ra_config.a_private_key
            self.interface['public_port'] = current_ra_config.a_public_port
            self.interface['address'] = current_ra_config.a_tunnel_cidr

            users = UserModel.query.all()
            for user in users:
                peer = WgPeer(
                    tunnel_cidr=user.tunnel_cidr,
                    public_key=user.public_key,
                    psk=user.psk,
                    keepalive=user.keepalive,
                    user_id=user.id,
                    username=user.username,
                    firstname=user.firstname,
                    surname=user.surname
                )
                self.peers.append(peer)
        else:
            print("There is no value present in the table!")


class WG_S2S(WG):
    """
    Helper class. This represents a S2S Wireguard config
    and makes all options and settings available.
    It handles loading and parsing of the current config from the database
    """

    def __init__(self, import_config=None, form=None):
        super().__init__(import_config=import_config, form=form)
        self.wg_interface = "wg0"

    def get_from_db(self):
        current_s2s_config = S2SConfig.query.get(1)
        if current_s2s_config:
            self.interface['private_key'] = current_s2s_config.a_private_key
            self.interface['public_port'] = current_s2s_config.a_public_port
            self.interface['address'] = current_s2s_config.a_tunnel_cidr

            peer = WgPeer(
                tunnel_cidr=current_s2s_config.b_tunnel_cidr,
                public_ip=current_s2s_config.b_public_ip,
                public_port=current_s2s_config.b_public_port,
                public_key=current_s2s_config.b_public_key,
                psk=current_s2s_config.psk,
                local_networks=current_s2s_config.b_local_networks,
                keepalive=current_s2s_config.keepalive
            )
            self.peers.append(peer)
        else:
            print("There is no value present in the table!")

    def import_config(self, config, form):
        # import the config to the DB
        S2SConfig().import_config(form, config)
        # read from DB
        self.get_from_db()


class WG_User(WG):
    """
    Helper class. This represents a RemoteAccess Peer (User) Wireguard config
    and makes all options and settings available.
    It handles loading and parsing of the current config from the database.
    """

    def __init__(self, user_id):
        self.user_id = user_id
        self.dns_ips = None
        self.wifi = None
        self.wifi_cidr = None
        super().__init__()
        self.wg_interface = "wg1"

    def get_from_db(self):
        current_rpi_config = UserModel.query.get(self.user_id)
        if current_rpi_config:
            self.interface['address'] = current_rpi_config.tunnel_cidr
            self.interface['private_key'] = current_rpi_config.private_key
            self.wifi_cidr = current_rpi_config.wifi_cidr
            current_ra_config = RAConfig.query.get(1)

            peer = WgPeer(
                tunnel_cidr=current_ra_config.a_tunnel_cidr,
                public_ip=modify_endpoint(current_ra_config.a_public_ip),
                public_port=current_ra_config.a_public_port,
                public_key=current_ra_config.a_public_key,
                local_networks=current_ra_config.a_local_networks,
                psk=current_rpi_config.psk,
                keepalive=current_rpi_config.keepalive
            )
            self.peers.append(peer)
            dns_list = [current_ra_config.dns1, current_ra_config.dns2]
            self.dns_ips = ",".join(filter(None, dns_list))
            self.wifi = current_rpi_config.wifi_password
        else:
            print("There is no value present in the table!")
