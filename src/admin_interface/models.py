from datetime import datetime

from flask_login import UserMixin
from flask_login._compat import unicode
from ipaddress import ip_network

from admin_interface import bcrypt
from db import db
from util import *


class VpnConfig(db.Model):
    """
    Abstract VpnConfig class
    Although this is a subclass of SQLAlchemy db.Model, it has no table in the database
    It is there to implement the method 'update_db', which both S2S and RAConfig share
    """
    __abstract__ = True

    def update_db_s2s(self, form):
        # Copies matching attributes from form onto S2S config
        config = self.__class__()
        form.populate_obj(config)

        # correct subnets if entered wrong ex. 10.1.2.34/24 10.1.2.0/24
        config.a_local_networks = correct_subnet(form.a_local_networks.data)
        config.b_local_networks = correct_subnet(form.b_local_networks.data)

        if not self.__class__().query.get(1):
            # There is no config in the DB

            # RAConfig has no b_private_key, b_public_key, psk, keepalive or iptables config. handle that here

            logging.info("PSK checkbox was set in form. Generating PSK")
            config.psk = wg_genpsk()

            logging.info("Keepalive was set in form.")
            config.keepalive = form.keepalive.data

            logging.info("L3-Config checkbox was set in form.")
            config.l3 = form.l3.data

            # Generate private and public keys
            config.a_private_key = wg_genkey()
            config.a_public_key = wg_pubkey(config.a_private_key)
            logging.info("Generated a_private_key, a_public_key")

            config.b_private_key = wg_genkey()
            config.b_public_key = wg_pubkey(config.b_private_key)
            logging.info("Generated a_private_key, a_public_key")

            db.session.add(config)
            logging.info("Write {} config to DB".format(self.__class__.__name__))
            db.session.commit()

        else:
            # There is already a config in the DB. update it
            self.update_form_fields(form)

    def update_db_ra(self, form):
        # Copies matching attributes from form onto RA config
        config = self.__class__()

        form.populate_obj(config)

        # correct subnets if entered wrong ex. 10.1.2.34/24 to 10.1.2.0/24
        config.a_local_networks = correct_subnet(form.a_local_networks.data)

        if not self.__class__().query.get(1):
            # There is no config in the DB

            # Generate private and public keys
            config.a_private_key = wg_genkey()
            config.a_public_key = wg_pubkey(config.a_private_key)
            logging.info("Generated a_private_key, a_public_key")

            logging.info("L3-Config checkbox was set in form.")
            config.l3 = form.l3.data

            db.session.add(config)
            logging.info("Write {} config to DB".format(self.__class__.__name__))
            db.session.commit()
        else:
            # There is already a config in the DB. update it
            self.update_form_fields(form)

    def update_form_fields(self, form):
        raise NotImplementedError("You should call this in the subclasses S2SConfig or RAConfig!")

    def as_dict(self):
        """
        :return: Return all attributes as JSON object. Skips the field "time_created"
        """
        ret = dict()
        for c in self.__table__.columns:
            if c.name == "time_created":
                continue
            ret[c.name] = getattr(self, c.name)
        return json.dumps(ret)

    def __repr__(self):
        return self.as_dict()


def correct_subnet(a_local_networks):
    if a_local_networks == '':
        return
    networks = [x.strip() for x in a_local_networks.split(",")]
    corrected_networks = []
    for net in networks:
        net = str(ip_network(net, strict=False))
        corrected_networks.append(net)
    return ','.join(corrected_networks)


class S2SConfig(VpnConfig):
    __tablename__ = 's2s_config'
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    time_created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    a_private_key = db.Column(db.String(100))
    a_public_key = db.Column(db.String(100))
    a_public_ip = db.Column(db.String(50), nullable=False)
    a_public_port = db.Column(db.Integer, nullable=False)
    a_tunnel_cidr = db.Column(db.String(50), nullable=False)
    a_local_networks = db.Column(db.Text)
    b_private_key = db.Column(db.String(100))
    b_public_key = db.Column(db.String(100))
    b_public_ip = db.Column(db.String(50), nullable=False)
    b_public_port = db.Column(db.Integer, nullable=False)
    b_tunnel_cidr = db.Column(db.String(50), nullable=False)
    b_local_networks = db.Column(db.Text)
    psk = db.Column(db.String(100))
    keepalive = db.Column(db.Integer, default=0)
    l3 = db.Column(db.Boolean, default=True)

    def update_form_fields(self, form):
        config = self.__class__()
        current_s2s_config = self.__class__().query.get(1)
        form.populate_obj(config)

        # Only copy the new fields, private/public key stay the same
        current_s2s_config.a_public_ip = config.a_public_ip
        current_s2s_config.a_public_port = config.a_public_port
        current_s2s_config.a_tunnel_cidr = config.a_tunnel_cidr
        # correct subnets if entered wrong ex. 10.1.2.34/24 10.1.2.0/24
        current_s2s_config.a_local_networks = correct_subnet(config.a_local_networks)

        current_s2s_config.b_public_ip = config.b_public_ip
        current_s2s_config.b_public_port = config.b_public_port
        current_s2s_config.b_tunnel_cidr = config.b_tunnel_cidr
        # correct subnets if entered wrong ex. 10.1.2.34/24 10.1.2.0/24
        current_s2s_config.b_local_networks = correct_subnet(config.b_local_networks)

        current_s2s_config.keepalive = config.keepalive
        current_s2s_config.l3 = config.l3

        db.session.commit()

    def import_config(self, form, js):
        """
        Import a S2S config into the database
        :param form: an instance of SiteToSiteSetupForm
        :param js: the imported JSON config
        """

        # Fill the SiteToSiteSetupForm and validate the inputs
        f = form
        f.a_public_ip.data = js['b_public_ip']
        f.a_public_port.data = js['b_public_port']
        f.a_tunnel_cidr.data = js['b_tunnel_cidr']
        f.a_local_networks.data = js['b_local_networks']

        f.b_public_ip.data = js['a_public_ip']
        f.b_public_port.data = js['a_public_port']
        f.b_tunnel_cidr.data = js['a_tunnel_cidr']
        f.b_local_networks.data = js['a_local_networks']

        f.psk.data = js['psk']
        f.keepalive.data = js['keepalive']

        # If the config is invalid in some way, abort
        if not f.validate():
            raise RuntimeError("Config file is not valid!")

        # Get the s2s_config from the DB if it exists
        config = S2SConfig.query.get(1)

        # If currently no config exists, create a new empty one and add it to the DB session
        if config is None:
            config = S2SConfig()
            db.session.add(config)

        # populate the config with the imported form data
        f.populate_obj(config)
        config.a_private_key = js['b_private_key']
        config.a_public_key = js['b_public_key']
        config.b_private_key = js['a_private_key']
        config.b_public_key = js['a_public_key']
        config.psk = js['psk']
        config.keepalive = js['keepalive']
        config.l3 = js['l3']

        # Write to DB
        logging.info("Importing {} config to DB".format(self.__class__.__name__))
        db.session.commit()

    def __repr__(self):
        return "S2S_CONFIG: {}".format(super().__repr__())


class RAConfig(VpnConfig):
    __tablename__ = 'ra_config'
    id = db.Column(db.Integer, default=1, primary_key=True)
    time_created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    a_private_key = db.Column(db.String(100))
    a_public_key = db.Column(db.String(100))
    a_public_ip = db.Column(db.String(50), nullable=False)
    a_public_port = db.Column(db.Integer, nullable=False)
    a_tunnel_cidr = db.Column(db.String(50), nullable=False)
    a_local_networks = db.Column(db.Text)
    dns1 = db.Column(db.String(50), default=None)
    dns2 = db.Column(db.String(50), default=None)
    l3 = db.Column(db.Boolean, default=True)
    user = db.relationship('UserModel', backref='user', lazy=True)

    def update_form_fields(self, form):
        config = self.__class__()
        current_ra_config = self.__class__().query.get(1)
        form.populate_obj(config)

        # Only copy the new fields, private/public key stay the same
        current_ra_config.a_public_ip = config.a_public_ip
        current_ra_config.a_public_port = config.a_public_port
        current_ra_config.a_tunnel_cidr = config.a_tunnel_cidr
        # correct subnets if entered wrong ex. 10.1.2.34/24 10.1.2.0/24
        current_ra_config.a_local_networks = correct_subnet(config.a_local_networks)
        current_ra_config.dns1 = config.dns1
        current_ra_config.dns2 = config.dns2
        current_ra_config.l3 = config.l3

        db.session.commit()

    def __repr__(self):
        return "RA_CONFIG: {}".format(super().__repr__())


class UserModel(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    time_created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    # user-specific config
    username = db.Column(db.String(50), nullable=False)
    firstname = db.Column(db.String(50), nullable=False)
    surname = db.Column(db.String(50), nullable=False)
    # wireguard-specific config
    tunnel_cidr = db.Column(db.String(50), nullable=False)
    public_key = db.Column(db.Text, nullable=False)
    private_key = db.Column(db.Text, nullable=False)
    psk = db.Column(db.String(100))
    keepalive = db.Column(db.Integer, default=0)
    ra_config_id = db.Column(db.Integer, db.ForeignKey('ra_config.id'))
    # raspberry-specific config
    wifi_password = db.Column(db.String(18), nullable=False)
    wifi_cidr = db.Column(db.String(50), nullable=False, default="192.168.4.1/24")

    def __repr__(self):
        return "User: {}".format(self.id)

    def setup_update_db(self, form):
        ra_user = UserModel()

        # Copies matching attributes from form onto ra_config
        form.populate_obj(ra_user)

        # Always set to the first remote access config in db
        # extendable if multiple remote access are allowed
        ra_user.ra_config_id = 1

        # Generate private, public keys
        ra_user.private_key = wg_genkey()
        ra_user.public_key = wg_pubkey(ra_user.private_key)
        if form.use_psk.data:
            ra_user.psk = wg_genpsk()

        # Generate wifi-password
        full_key = wg_genkey()
        ra_user.wifi_password = full_key[:18]

        # Write model to DB
        db.session.add(ra_user)
        db.session.commit()
        db.session.flush()

        return ra_user.id

    def edit_update_db(self, form):
        # Copies matching attributes from form onto ra_config
        self.username = form.username.data
        self.firstname = form.firstname.data
        self.surname = form.surname.data
        self.tunnel_cidr = form.tunnel_cidr.data
        if form.use_psk.data:
            if not self.psk:
                self.psk = wg_genpsk()
        else:
            self.psk = None
        self.keepalive = form.keepalive.data

        # Write model to DB
        db.session.commit()


class LoginModel(db.Model, UserMixin):
    __tablename__ = 'login'
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    username = db.Column(db.String(50), nullable=False)
    password = db.Column(db.String(100), nullable=False)
    remember = db.Column(db.Boolean, default=False)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def set_password(self, password):
        """Create hashed password."""
        admin_user = self.__class__().query.get(self.id)
        self.password = bcrypt.generate_password_hash(password)
        admin_user.password = self.password
        db.session.commit()

    def check_password(self, password):
        """Check hashed password."""
        return bcrypt.check_password_hash(self.password, password)


def delete(model, db_id):
    entry = model.query.get_or_404(db_id)
    db.session.delete(entry)
    db.session.commit()
    flash("Item was deleted successfully!", "success")
