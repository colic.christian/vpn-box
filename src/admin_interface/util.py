import binascii
import json
import logging
import os.path
import shlex
import subprocess
import tempfile

import geoip2.database
import nacl.bindings
import pyqrcode
import wifi_qrcode_generator
from flask import flash
from geoip2.errors import AddressNotFoundError
from jinja2 import Environment, FileSystemLoader

git_revision = ""
jinja_env = Environment(
    loader=FileSystemLoader(os.path.join(os.getcwd(), "admin_interface/config_templates")),
    keep_trailing_newline=True
)


def run_cmd(cmd):
    """
    Run a command on the local system
    :param cmd: The command to run
    :return: CompletedProcess instance
    """
    logging.info("Running command with subprocess: '{}'".format(cmd))
    cp = subprocess.run(shlex.split(cmd), universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if cp.returncode != 0:
        raise RuntimeError(cp.stderr)
    return cp


class WgQuick:
    """
    Helper class for usage of wg-quick
    Start, Stop, Restart of WG interfaces
    Getting the current state of a WG interface
    """

    @staticmethod
    def down(interface):
        try:
            run_cmd("sudo wg-quick down {}".format(interface))
            flash('Wireguard Interface: {interface} stopped'.format(**locals()), 'success')
        except RuntimeError as e:
            print('wg-quick error: {e}'.format(**locals()))
            # uncomment the next line if user wants more advanced flash messages
            # flash('{e}'.format(**locals()), 'info')

    @staticmethod
    def up(interface):
        try:
            run_cmd("sudo wg-quick up {}".format(interface))
            flash('Wireguard Interface: {interface} started'.format(**locals()), 'success')
        except RuntimeError as e:
            print('wg-quick error: {e}'.format(**locals()))
            # uncomment the next line if user wants more advanced flash messages
            # flash('{e}'.format(**locals()), 'info')

    @staticmethod
    def restart(interface):
        state = WgQuick.get_state(interface)
        if state == "up":
            WgQuick.down(interface)
            WgQuick.up(interface)
        if state == "down":
            WgQuick.up(interface)

    @staticmethod
    def get_state(interface):
        """
        Get the current status of Wireguard
        :param interface: which wg interface to check
        :return: "up" or "down"
        """
        try:
            cp = run_cmd("sudo wg show {}".format(interface))
            if cp.returncode == 1:
                return "down"
            else:
                return "up"
        except RuntimeError as e:
            logging.info('wg-quick error: {e}'.format(**locals()))
            return "down"


class GeoIP:
    """
    Helper class for MaxMind GeoIP lookups
    Use with a 'with' statement:
    with GeoIP() as geo:
        geo.lookup_city("1.1.1.1")

    As this will make sure that the reade object is closed properly
    """

    def __init__(self):
        self.reader = geoip2.database.Reader("/opt/vpn-box/GeoLite2-City.mmdb")

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.reader.close()

    def lookup(self, ip):
        return self.reader.city(ip)

    def lookup_city(self, ip):
        return self.lookup(ip).city.name

    def lookup_country(self, ip):
        return self.lookup(ip).country.name

    def lookup_country_iso_code(self, ip):
        try:
            return self.lookup(ip).country.iso_code
        except (ValueError, AddressNotFoundError):
            # if the IP if invalid or not found in the DB
            return "_unknown"


class Iptables:
    """
    Helper class for interacting with IPv4 and IPv6 iptables
    There is a defined section in the ruleset files. The functions of this class look for that block
    and add or remove rules in that section
    """
    start_string = "# START: Allow Wireguard - Managed by Flask - Do not change manually!\n"
    end_string = "# END: Allow Wireguard\n"

    @staticmethod
    def add_rule_wg(port):
        """
        Add a rule to the ipv4 and ipv6 iptables ruleset for wireguard
        This will insert a rule in the INPUT chain, allowing UDP connections on the given port
        :param port: UDP port to allow
        """
        for iptables_ruleset in ['iptables', 'ip6tables']:
            with tempfile.TemporaryDirectory() as temp_dir:
                run_cmd("cp /etc/sysconfig/{iptables_ruleset} {temp_dir}".format(**locals()))
                f = open(temp_dir + "/" + iptables_ruleset, "r")
                contents = f.readlines()
                f.close()
                insert_index = -1
                for i, line in enumerate(contents):
                    if line == Iptables.start_string:
                        insert_index = i + 1
                        break

                rule = "-A INPUT -p udp -m udp --dport {} -j ACCEPT\n".format(port)
                contents.insert(insert_index, rule)

                f = open(temp_dir + "/" + iptables_ruleset, "w")
                contents = "".join(contents)
                f.write(contents)
                f.close()
                run_cmd("sudo cp {} /etc/sysconfig/{}".format(temp_dir + "/" + iptables_ruleset, iptables_ruleset))
        Iptables.restart()

    @staticmethod
    def delete_rule_wg(port):
        """
        Remove the rule which allowed UDP connections on the given port
        :param port: UDP port
        """
        rule = "-A INPUT -p udp -m udp --dport {} -j ACCEPT\n".format(port)
        if Iptables.exists_rule_wg(port):
            for iptables_ruleset in ['iptables', 'ip6tables']:
                with tempfile.TemporaryDirectory() as temp_dir:
                    run_cmd("cp /etc/sysconfig/{iptables_ruleset} {temp_dir}".format(**locals()))
                    f = open(temp_dir + "/" + iptables_ruleset, "r")
                    contents = f.readlines()
                    f.close()
                    delete_index = -1
                    for i, line in enumerate(contents):
                        if line == rule:
                            delete_index = i

                    contents.pop(delete_index)

                    f = open(temp_dir + "/" + iptables_ruleset, "w")
                    contents = "".join(contents)
                    f.write(contents)
                    f.close()
                    run_cmd("sudo cp {} /etc/sysconfig/{}".format(temp_dir + "/" + iptables_ruleset, iptables_ruleset))
            Iptables.restart()
        else:
            logging.info("iptables and ip6tables rules have not been deleted as they do not exist!")
            flash("iptables/ip6tables rules have not been deleted, please check manually!", "danger")

    @staticmethod
    def exists_rule_wg(port):
        """
        Check if a rule for the given UDP port exists in the INPUT chain
        :param port: UDP port
        """
        rule = "-A INPUT -p udp -m udp --dport {} -j ACCEPT\n".format(port)
        exist_iptables = False
        exist_ip6tables = False

        # check whether rule exist in iptables
        f = open("/etc/sysconfig/iptables", "r")
        contents = f.readlines()
        f.close()
        for i, line in enumerate(contents):
            if line == rule:
                exist_iptables = True
                logging.info("iptables rule exists")
            else:
                logging.info("iptables rule does not exist")

        # check whether rule exist in ip6tables
        f = open("/etc/sysconfig/ip6tables", "r")
        contents = f.readlines()
        f.close()
        for i, line in enumerate(contents):
            if line == rule:
                exist_ip6tables = True
                logging.info("ip6tables rule exists")
            else:
                logging.info("ip6tables rule does not exist")

        if exist_iptables and exist_ip6tables:
            logging.info("both: iptables and ip6tables rule exists")
            return True
        else:
            logging.info("both: iptables and ip6tables rule does NOT exist")
            return False

    @staticmethod
    def restart():
        """
        Restarts both iptables and ip6tables services
        """
        run_cmd("sudo systemctl restart iptables")
        run_cmd("sudo systemctl restart ip6tables")


class SystemLogs:
    @staticmethod
    def get_log_file(log_type, lines):
        """
        Returns the last n lines of the specified log file
        """
        try:
            if log_type == "ovs":
                return run_cmd("sudo tail -n {} /var/log/openvswitch/ovs-vswitchd.log".format(lines)).stdout
            if log_type == "vpn-box":
                return run_cmd("sudo tail -n {} /var/log/vpn-box.log".format(lines)).stdout
            if log_type == "iptables":
                return run_cmd("sudo tail -n {} /var/log/iptables.log".format(lines)).stdout
            if log_type == "syslog":
                return run_cmd("sudo tail -n {} /var/log/messages".format(lines)).stdout
        except RuntimeError:
            return ""


def patch_file(path, content):
    """
    Overwrite a local config file
    If Environment var ENV equals "dev", then all outputs will be redirected to /tmp/vpn-box/
    :param path: The file to patch
    :param content: Content of the file
    """
    patch_path = path
    # todo: remove dev
    if os.getenv('ENV') == 'dev':
        logging.info("Redirecting patch_file to /tmp/vpn-box/ because ENV=dev")
        patch_path = "/tmp/vpn-box/" + path
        if not os.path.exists("/tmp/vpn-box"):
            os.mkdir("/tmp/vpn-box")

    logging.info("Patching file '{}' on host system".format(patch_path))
    # Write the rendered jinja template to a temporary file and then copy it to the destination
    with tempfile.TemporaryDirectory() as temp_dir:
        with open(os.path.join(temp_dir, 'tmp.txt'), 'a') as tmp_file:
            tmp_file.write(content)
        run_cmd("sudo mkdir -p {}".format(os.path.dirname(patch_path)))
        run_cmd("sudo cp {} {}".format(os.path.join(temp_dir, 'tmp.txt'), patch_path))


def wg_genkey():
    """
    Function to generate a Wireguard private key

    Returns:
        string: A base64 encoded Wireguard private key
    """
    secret = bytearray(nacl.bindings.randombytes(32))
    # curve25519 normalize secret
    secret[0] &= 248
    secret[31] &= 127
    secret[31] |= 64
    return binascii.b2a_base64(bytes(secret)).decode('ascii').strip()


def wg_pubkey(secret):
    """
    Given a private key, compute the public key

    Parameters:
        secret (string): The base64 encoded private key

    Returns:
        string: A base64 encoded Wireguard public key
    """
    key_bytes = binascii.a2b_base64(secret)
    public_key_bytes = nacl.bindings.crypto_scalarmult_base(key_bytes)
    return binascii.b2a_base64(public_key_bytes).decode('ascii').strip()


def wg_genpsk():
    """
    Function to generate a Wireguard pre-shared key

    Returns:
        string: A base64 encoded Wireguard pre-shared key
    """
    return wg_genkey()


def inject_global_variable(app, var_dict):
    """
    Inject global variable into the app context, which can then be used in jinja templates
    :param app: The flask app instance
    :param var_dict: a dictionary of key-value pairs that will be injected as global vars.
    example: dict(git_revision="abcdefg")
    """

    @app.context_processor
    def inject_stage_and_region():
        return var_dict


def get_git_revision_short_hash():
    """
    Get the current Git commit hash of HEAD
    :return: the short commit hash
    """
    try:
        return run_cmd("git rev-parse --short HEAD").stdout
    except RuntimeError:
        return "local dev version"


def read_and_parse_s2s_config(filename):
    """
    Open and read the uploaded s2s config file
    Try to parse it
    Return the loaded JSON object
    """
    # open for reading
    with open(os.path.join("uploads/", filename)) as uploaded_config:
        try:
            content = uploaded_config.read()

            # parse it
            if content.startswith("S2S_CONFIG:"):
                content = content.replace("S2S_CONFIG: ", "")
                return json.loads(content)
            else:
                raise Exception('Provided file has wrong content {}'.format(content))
        except Exception:
            flash("The provided file was wrong!", "danger")
            raise Exception("Wrong File")


def create_wg_qr_code_image(file):
    qr = pyqrcode.create(file)
    qr.png("admin_interface/static/qrcode/wg_qrcode.png", scale=4)


def create_wifi_qr_code_image(ssid, password):
    """
    Provide ssid and password to generate qr-code image in admin_interface/static/qrcode/wifi_qrcode.png
    :param ssid: SSID of WiFi
    :param password: password to access WiFi
    example: create_wifi_qr_code_image(WiFi-VPN L3, qwerasdf)
    """
    qr = wifi_qrcode_generator.wifi_qrcode(ssid, False, 'WPA', password)
    qr.save("admin_interface/static/qrcode/wifi_qrcode.png", 'PNG')


def modify_endpoint(endpoint):
    if ":" in endpoint:
        return "[" + endpoint + "]"
    else:
        return endpoint
